public class Machine {
    private String name;
    private int maxwater;
    private int maxcoffee;
    private int maxtank;
    private boolean onOff = false;
    private int tank = 0;
    private int coffee;
    private int water;
    private final int amewater = 100;
    private final int expwater = 30;
    private final int amecoffee = 22;
    private final int expcoffee = 22;


    public int getAmewater() {
        return amewater;
    }

    public int getExpwater() {
        return expwater;
    }

    public int getAmecoffee() {
        return amecoffee;
    }

    public int getExpcoffee() {
        return expcoffee;
    }

    public Machine(String name, int maxwater, int maxcoffee, int maxtank) {
        this.name = name;
        this.maxwater = maxwater;
        this.maxcoffee = maxcoffee;
        this.maxtank = maxtank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxtank() {
        return maxtank;
    }

    public void setMaxtank(int maxtank) {
        this.maxtank = maxtank;
    }

    public int getCoffee() {
        return coffee;
    }

    public void setCoffee(int coffee) {
        this.coffee = coffee;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public int getMaxwater() {
        return maxwater;
    }

    public void setMaxwater(int maxwater) {
        this.maxwater = maxwater;
    }

    public int getMaxcoffee() {
        return maxcoffee;
    }

    public void setMaxcoffee(int maxcoffee) {
        this.maxcoffee = maxcoffee;
    }

    public int getTank() {
        return tank;
    }

    public void setTank(int tank) {
        this.tank = tank;
    }

    public boolean getOnOff() {
        return onOff;
    }

    public void setOnOff(boolean onOff) {
        this.onOff = onOff;
    }
}
