import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class hw03task11 {
    public static void main(String[] args) throws IOException {
        System.out.print("Enter a, b, c: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String first, second, third;
        String[] data = reader.readLine().split(" ");
        first = data[0];
        second = data[1];
        third = data[2];
        double a = Integer.parseInt(first);
        double b = Integer.parseInt(second);
        double c = Integer.parseInt(third);
        double d = Math.pow(b, 2) - 4 * a * c;
        if (d > 0) {
            double x1, x2;
            x1 = (-b + Math.sqrt(d)) / (2 * a);
            x2 = (-b - Math.sqrt(d)) / (2 * a);
            System.out.println("Roots of the equation: x1 = " + x1 + ", x2 = " + x2);
        } else if (d == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Equation has a single root: x = " + x);
        } else {
            System.out.println("The equation has no real roots.");


        }
    }
}