public class Hourly extends Employee {
    private int time;
    private int timepay;
    private int tax = 20;

    @Override
    public int getTax() {
        return tax;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTimepay() {
        return timepay;
    }

    public void setTimepay(int timepay) {
        this.timepay = timepay;
    }

    public Hourly(String fio, boolean children, String payment, boolean off, int time, int timepay) {
        super(fio, children, payment, off);
        this.time=time;
        this.timepay=timepay;
    }

    public int personalEarning() {
        return time * timepay;
    }
}
