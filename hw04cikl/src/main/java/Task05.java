import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double i = Double.parseDouble(scanner.next());
        System.out.println(i);
        if (i >= 100000000 && i < 1000000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x6 = (i % 1000000 - i % 100000) / 100000;
            double x7 = (i % 10000000 - i % 1000000) / 1000000;
            double x8 = (i % 100000000 - i % 10000000) / 10000000;
            double x9 = (i - i % 100000000) / 100000000;
            if (x1 == x9) {
                if (x2 == x8) {
                    if (x3 == x7) {
                        if (x4 == x6) {
                            System.out.println("Yes");
                        }
                    }
                }
            } else System.out.println("No.");
        } else if (i >= 10000000 && i < 100000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i % 1000000 - i % 100000) / 100000;
            double x7 = (i % 10000000 - i % 1000000) / 1000000;
            double x8 = (i - i % 10000000) / 10000000;
            if (x1 == x8) {
                if (x2 == x7) {
                    if (x3 == x6) {
                        if (x4 == x5) {
                            System.out.println("Yes");
                        }
                    }
                }
            } else System.out.println("No.");
        } else if (i >= 1000000 && i < 10000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i % 1000000 - i % 100000) / 100000;
            double x7 = (i - i % 1000000) / 1000000;
            if (x1 == x7) {
                if (x2 == x6) {
                    if (x3 == x5) {
                        System.out.println("Yes");
                    }
                }
            } else System.out.println("No.");
        } else if (i >= 100000 && i < 1000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i - i % 100000) / 100000;
            if (x1 == x6) {
                if (x2 == x5) {
                    if (x3 == x4) {
                        System.out.println("Yes");
                    }
                }
            } else System.out.println("No.");
        } else if (i >= 10000 && i < 100000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i - i % 10000) / 10000;
            if (x1 == x5) {
                if (x2 == x4) {
                    System.out.println("Yes");
                }
            } else System.out.println("No.");
        } else if (i >= 1000 && i < 10000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i - i % 1000) / 1000;
            if (x1 == x4) {
                if (x2 == x3) {
                    System.out.println("Yes");
                }
            } else System.out.println("No.");
        } else if (i >= 100 && i < 1000) {
            double x1 = i % 10;
            double x3 = (i - i % 100) / 100;
            if (x1 == x3) {
                System.out.println("Yes");
            } else System.out.println("No.");
        } else if (i >= 10 && i < 100) {
            double x1 = i % 10;
            double x2 = (i - i % 10) / 10;
            if (x1 == x2) {
                System.out.println("Yes");
            } else System.out.println("No.");
        }
    }
}
