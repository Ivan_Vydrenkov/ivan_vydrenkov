import java.util.Scanner;

public class hw03task03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Choose an animal:");
        System.out.println("1. Cat");
        System.out.println("2. Dog");
        System.out.println("3. Pig");
        System.out.println("4. Cow");
        System.out.println("5. Frog");
        System.out.println("6. Duck");
        System.out.println("7. Bird");
        System.out.println("8. Snake");
        System.out.println("9. Tiger");
        System.out.println("10. Fish");
        int animal = in.nextInt();
        switch (animal) {
            case 1:
                System.out.println("Мяу!");
                break;
            case 2:
                System.out.println("Гав!");
                break;
            case 3:
                System.out.println("Хрю!");
                break;
            case 4:
                System.out.println("Му!");
                break;
            case 5:
                System.out.println("Ква!");
                break;
            case 6:
                System.out.println("Кря!");
                break;
            case 7:
                System.out.println("Чирик!");
                break;
            case 8:
                System.out.println("Тс-с-с...!");
                break;
            case 9:
                System.out.println("Р-р-р...!");
                break;
            case 10:
                System.out.println("Буль");
                break;
            default:
                System.out.println("Invalid number entered");
        }
    }
}
