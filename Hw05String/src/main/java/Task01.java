import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int length = str.length();
        char chr = scanner.next().charAt(0);
        int sum = 0;
        int index = 0;
        for (index = 0; index <= length; ) {
            if (str.indexOf(chr) < 0) {
                System.out.println("Symbol not found.");
                break;
            } else
                index = str.indexOf(chr, index);
            if (index < 0) {
                break;
            }
            System.out.println(index);
            index = index + 1;
            sum = sum + 1;
        }
        System.out.println(sum);
    }
}
