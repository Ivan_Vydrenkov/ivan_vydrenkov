import java.util.Scanner;

//Нужно создать программу, используя методы,
//        которая поможет рассчитать минимальное
//        количество копейщиков, которое необходимо, чтобы убить дракона.

public class Task05 {

    public static int dragonAttack(int allhp, int ddamage) {
        int losthp = allhp - ddamage;
        return losthp;
    }

    public static int allHp(int soldiersum, int solhp) {
        int allhp = soldiersum * solhp;
        return allhp;
    }

    public static int soldierAttack(int dhp, int alldamage) {
        dhp = dhp - alldamage;
        return dhp;
    }

    public static int allDamage(int soldiersum, int onesoldamage) {
        int alldamage = soldiersum * onesoldamage;
        return alldamage;
    }

    public static int lostSoldierSum(int allhp, int solhp) {
        if (allhp <= 0) {
            return 0;
        }
        int lostsol = allhp / solhp;
        if (allhp % solhp > 0) {
            lostsol++;
        }
        return lostsol;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Dragon hp is: ");
        int dhp = scanner.nextInt();
        int dhp2 = dhp;

        System.out.print("Dragon damage is: ");
        int ddamage = scanner.nextInt();

        System.out.print("soldier hp is: ");
        int solhp = scanner.nextInt();

        System.out.print("soldier damage is: ");
        int onesoldamage = scanner.nextInt();

        int all = 0;

        for (int i = 1; dhp > 0; i++) {
            int soldiersum = i;

            for (; dhp > 0; ) {
                int alldamage = allDamage(soldiersum, onesoldamage);
                dhp = soldierAttack(dhp, alldamage);
                int allhp = allHp(soldiersum, solhp);
                allhp = dragonAttack(allhp, ddamage);
                soldiersum = lostSoldierSum(allhp, solhp);
                if (soldiersum == 0)
                    break;
            }
            if (soldiersum == 0) {
                dhp = dhp2;
            }
            all++;
        }
        System.out.println("You need " + all + " soldiers.");
    }
}
