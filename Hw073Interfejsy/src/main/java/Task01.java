public class Task01 {
    public static void main(String[] args) {
        //        task01 Машина варит Американо и Эспрессо:
        ExpressoAmericano model01 = new ExpressoAmericano("Model01", 2000, 500, 500);
        model01.setWater(2000);
        model01.setCoffee(500);
        ButtonOn.onMachine(model01);
        model01.cookAmerikani(model01);
        model01.cookExpresso(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.putWater(model01, 1000);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.cookAmerikani(model01);
        model01.clearTank(model01);
        model01.cookAmerikani(model01);
        model01.putCoffee(model01, 300);
        model01.cookAmerikani(model01);
        ButtonOff.offMachine(model01);
        model01.cookAmerikani(model01);
    }
}
