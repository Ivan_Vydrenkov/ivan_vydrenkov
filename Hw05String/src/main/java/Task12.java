import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String[] wordArray = text.split(" ");
        Pattern pattern = Pattern.compile("^[;:].*[[]]()[]]]$");
        for (String word : wordArray) {
            Matcher matcher = pattern.matcher(word);
            if (matcher.find()) {
                int length = word.length() - 1;
                char first = word.charAt(0);
                if (word.charAt(1) != first) {
                    char last = word.charAt(length);
                    String word2 = word.replace(first, '1');
                    word2 = word2.replace(last, '1');
                    word2 = word2.replace("-", "");
                    word2 = word2.replace("1", "");
                    if (word2.equals("")) {
                        System.out.println(word);
                    }
                }
            }
        }
    }
}
