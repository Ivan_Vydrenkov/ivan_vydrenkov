public class Ticket {
    private int num;
    private String name;
    private String firstname;
    private int grnum;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getGrnum() {
        return grnum;
    }

    public void setGrnum(int grnum) {
        this.grnum = grnum;
    }

    public Ticket (int num, String name, String firstname, int grnum){
        this.num=num;
        this.name=name;
        this.firstname=firstname;
        this.grnum=grnum;
    }
}
