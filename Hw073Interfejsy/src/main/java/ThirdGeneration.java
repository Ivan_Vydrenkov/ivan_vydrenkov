public interface ThirdGeneration {
    void showMessageWater();
    void showMessageCoffee();
    void showMessageTank();
    void showMessageMilk();
    void showMessageBeansCoffee();

    Object getAmericano();
    Object getLatte();

    void clearTank(Machine x);
    void putCoffee(Machine x, int coffee);
    void putWater(Machine x, int water);
    void putMilk(Machine x, int milk);
    void putBeansCoffee(Machine x, int beanscoffee);

    Americano cookAmerikani (Machine x);
    Latte cookLatte (Machine x);

    boolean chekTank (Machine x, int coffeeOneCup);
    boolean chekWater (Machine x, int waterOneCup);
    boolean chekMilk (Machine x, int milkOneCup);
    boolean chekBeansTank (Machine x, int coffeeOneCup);
}
