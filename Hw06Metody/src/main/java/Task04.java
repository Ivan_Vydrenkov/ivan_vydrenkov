import java.util.Random;
import java.util.Scanner;

//Написать метод:
//        выводящий значения треугольника на консоль в таком виде как на рисунке;
//        вычисляющий наибольшую с умму чисел, через которые проходит путь, начинающийся на вершине и заканчивающийся где-то на основании.

public class Task04 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the triangle height");
        int height = scan.nextInt();

        int[][] triangle = generateTriangle(height);
        printTriangle(triangle);

        int res = findMaxWay(triangle);
        System.out.println("\n" + "Maximum way= " + res);
    }

    public static int[][] generateTriangle(int n) {
        Random random = new Random();
        int[][] triangle = new int[n][];

        for (int i = 0; i < n; i++) {
            triangle[i] = new int[i + 1];
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i + 1; j++) {
                triangle[i][j] = random.nextInt(100);
            }
        }
        return triangle;
    }

    public static void printTriangle(int[][] arr) {

        int N = arr.length;

        for (int i = 0; i < N; i++) {
            for (int p = 0; p < 2 * (N - i - 1); p++) {
                System.out.print(" ");
            }
            for (int k = 0; k < arr[i].length; k++) {
                if (arr[i][k] < 10) {
                    System.out.print(" " + arr[i][k] + "  ");
                } else {
                    System.out.print(arr[i][k] + "  ");
                }
            }
            System.out.println();
        }
    }

    public static int findMaxWay(int[][] a) {

        String way[] = new String[a.length - 1];
        for (int i = a.length - 2; i >= 0; i--) {
            for (int j = 0; j < i + 1; j++) {
                a[i][j] += Math.max(a[i + 1][j], a[i + 1][j + 1]);

                if (a[i + 1][j] > a[i + 1][j + 1]) way[i] = "left";
                else way[i] = "right";
            }
        }
        for (int i = 0; i < way.length; i++) {
            System.out.print(way[i] + " ");

        }
        return a[0][0];
    }

}
