import java.util.Scanner;

public class СappuccinoLatte extends Machine implements SecondGeneration {
    private int maxmilk;
    private int milk = 0;
    private final int capcoffee = 22;
    private final int capwater = 30;
    private int milkonecup;

    public int getMilkonecup() {
        return milkonecup;
    }

    public void setMilkonecup(int milkonecup) {
        this.milkonecup = milkonecup;
    }

    public int getCapcoffee() {
        return capcoffee;
    }

    public int getCapwater() {
        return capwater;
    }

    public int getMaxmilk() {
        return maxmilk;
    }

    public void setMaxmilk(int maxmilk) {
        this.maxmilk = maxmilk;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public СappuccinoLatte(String name, int maxwater, int maxcoffee, int maxtank, int maxmilk) {
        super(name, maxwater, maxcoffee, maxtank);
        this.maxmilk = maxmilk;
    }

    public void showMessageWater() {
        System.out.println("Not enough water.");
    }

    public void showMessageCoffee() {
        System.out.println("Not enough coffee.");
    }

    public void showMessageTank() {
        System.out.println("The tank is full.");
    }

    public void showMessageMilk() {
        System.out.println("Not enough milk.");
    }

    public Expresso getExpresso() {
        System.out.println("Take your Expresso.");
        return new Expresso();
    }

    public Americano getAmericano() {
        System.out.println("Take your Americano.");
        return new Americano();
    }

    public Cappuccino getCappuccino() {
        System.out.println("Take your Cappuccino.");
        return new Cappuccino();
    }

    public Latte getLatte() {
        System.out.println("Take your Latte.");
        return new Latte();
    }

    public void clearTank(Machine x) {
        setTank(0);
    }

    public void putCoffee(Machine x, int coffee) {
        setCoffee(coffee);
    }

    public void putWater(Machine x, int water) {
        setWater(water);
    }

    public void putMilk(Machine x, int milk) {
        setMilk(milk);
    }

    public Expresso cookExpresso(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, x.getExpcoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, x.getExpcoffee()) == false) {
            return null;
        }
        if (chekWater(x, x.getExpwater()) == false) {
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getExpcoffee());
        x.setWater(x.getWater() - x.getExpwater());
        x.setTank(x.getTank() + x.getExpcoffee());
        return getExpresso();
    }

    public Americano cookAmerikani(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, x.getAmecoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, x.getAmecoffee()) == false) {
            return null;
        }
        if (chekWater(x, x.getAmewater()) == false) {
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getAmecoffee());
        x.setWater(x.getWater() - x.getAmewater());
        x.setTank(x.getTank() + x.getAmecoffee());
        return getAmericano();
    }

    public Cappuccino cookCappuccino(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, getCapcoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, getCapcoffee()) == false) {
            return null;
        }
        if (chekWater(x, getCapwater()) == false) {
            return null;
        }
        System.out.println("How much milk to add to coffee??");
        Scanner scn = new Scanner(System.in);
        int milkuse = scn.nextInt();
        setMilkonecup(milkuse);
        if (chekMilk(x, getMilkonecup()) == false){
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getExpcoffee());
        x.setWater(x.getWater() - x.getExpwater());
        x.setTank(x.getTank() + x.getExpcoffee());
        setMilk(getMilk() - getMilkonecup());
        return getCappuccino();
    }

    public Latte cookLatte(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, getCapcoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, getCapcoffee()) == false) {
            return null;
        }
        if (chekWater(x, getCapwater()) == false) {
            return null;
        }
        System.out.println("How much milk to add to coffee??");
        Scanner scn = new Scanner(System.in);
        int milkuse = scn.nextInt();
        setMilkonecup(milkuse);
        if (chekMilk(x, getMilkonecup()) == false){
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getExpcoffee());
        x.setWater(x.getWater() - x.getExpwater());
        x.setTank(x.getTank() + x.getExpcoffee());
        setMilk(getMilk() - getMilkonecup());
        return getLatte();
    }

    public boolean chekTank(Machine x, int coffeeOneCup) {
        if (x.getTank() + coffeeOneCup > x.getMaxtank()) {
            showMessageTank();
            System.out.println(x.getTank() + coffeeOneCup + x.getMaxtank());
            return false;
        }
        return true;
    }

    public boolean chekWater(Machine x, int waterOneCup) {
        if (waterOneCup > x.getWater()) {
            showMessageWater();
            return false;
        }
        return true;
    }

    public boolean chekCoffee(Machine x, int coffeeOneCup) {
        if (coffeeOneCup > x.getCoffee()) {
            showMessageCoffee();
            return false;
        }
        return true;
    }

    public boolean chekMilk(Machine x, int milkOneCup) {
        if (milk < milkonecup){
            showMessageMilk();
            return false;
        }
        return true;
    }
}
