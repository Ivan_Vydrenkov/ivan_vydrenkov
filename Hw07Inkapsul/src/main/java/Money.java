public class Money {
    private long rub;
    private byte cents;

    public long getRub() {
        return rub;
    }

    public void setRub(long rub) {
        this.rub = rub;
    }

    public byte getCents() {
        return cents;
    }

    public void setCents(byte cents) {
        this.cents = cents;
    }

    public Money(long x, byte y) {
        this.cents = y;
        this.rub = x;
    }

    public static void toString(Money x, Money y) {
        System.out.println("You have " + convert(x)+ " and " + convert(y) + " rubs.");
    }

    public static double convert(Money x) {
        double rub = (double) x.rub;
        double cents = (double) x.cents;
        double a = rub + cents/100;
        System.out.println(a);
        return a;
    }

    public static void sumMoney (Money x, Money y){
        double one = convert(x);
        double two = convert(y);
        System.out.println("The sum is " + (one + two));
    }

    public static void subtractionMoney (Money x, Money y){
        double one = convert(x);
        double two = convert(y);
        System.out.println("The subtraction is " + (one - two));
    }

    public static void divisionMoney (Money x, Money y){
        double one = convert(x);
        double two = convert(y);
        System.out.println("The division is " + (one / two));
    }

    public static void divisionOnShareMoney (Money x, Money y){
        double one = convert(x);
        double two = convert(y);
        double end = (one + two) / 0.5;
        System.out.println("The division sum on 0.5 is " + end);
    }

    public static void comparisonMoney (Money x, Money y){
        double one = convert(x);
        double two = convert(y);
        if (one < two ){
            System.out.println(two + "more than" + one);
        }
        else if (one > two){
            System.out.println(one + "more than" + two);
        }
        else {
            System.out.println("Equal");
        }

    }
}
