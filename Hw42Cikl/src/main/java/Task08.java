import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long i = scanner.nextLong();
        if (i < 10) {
            System.out.println(1);
            System.out.println(i);
        } else {
            long ost = i % 10;
            long num = ost;
            long dex = 10;
            long num2;
            long sum = ost;
            long max = 1;
            for (int ten = 100; ten <= i * 10; ten = ten * 10) {
                num2 = i % ten - num;
                ost = num2 / dex;
                num = num + num2;
                dex = dex * 10;
                sum = sum + ost;
                max++;
            }
            System.out.println("");
            System.out.println(max);
            System.out.println(sum);
        }
    }
}
