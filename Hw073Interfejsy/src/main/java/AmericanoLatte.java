import java.util.Scanner;

public class AmericanoLatte extends Machine implements ThirdGeneration {
    private int maxmilk;
    private int milk = 0;
    private final int lattecoffee = 22;
    private final int lattewater = 30;
    private int milkonecup;
    private int maxbeanstank;
    private int beanstank = 0;

    public AmericanoLatte(String name, int maxwater, int maxcoffee, int maxtank, int maxmilk, int maxbeanstank) {
        super(name, maxwater, maxcoffee, maxtank);
        this.maxmilk = maxmilk;
        this.maxbeanstank = maxbeanstank;
    }

    public int getMaxmilk() {
        return maxmilk;
    }

    public void setMaxmilk(int maxmilk) {
        this.maxmilk = maxmilk;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public int getLattecoffee() {
        return lattecoffee;
    }

    public int getLattewater() {
        return lattewater;
    }

    public int getMilkonecup() {
        return milkonecup;
    }

    public void setMilkonecup(int milkonecup) {
        this.milkonecup = milkonecup;
    }

    public int getMaxbeanstank() {
        return maxbeanstank;
    }

    public void setMaxbeanstank(int maxbeanstank) {
        this.maxbeanstank = maxbeanstank;
    }

    public int getBeanstank() {
        return beanstank;
    }

    public void setBeanstank(int beanstank) {
        this.beanstank = beanstank;
    }

    public void showMessageWater() {
        System.out.println("Not enough water.");
    }

    public void showMessageCoffee() {
        System.out.println("Not enough coffee.");
    }

    public void showMessageTank() {
        System.out.println("The tank is full.");
    }

    public void showMessageMilk() {
        System.out.println("Not enough milk.");
    }

    public void showMessageBeansCoffee() {
        System.out.println("Not enough beans.");
    }

    public Americano getAmericano() {
        System.out.println("Take your Americano.");
        return new Americano();
    }

    public Latte getLatte() {
        System.out.println("Take your Latte.");
        return new Latte();
    }

    public void clearTank(Machine x) {
        setTank(0);
    }

    public void putCoffee(Machine x, int coffee) {
        setCoffee(coffee);
    }

    public void putWater(Machine x, int water) {
        setWater(water);
    }

    public void putMilk(Machine x, int milk) {
        setMilk(milk);
    }

    public void putBeansCoffee(Machine x, int beanscoffee) {
        setBeanstank(beanscoffee);
    }

    public Americano cookAmerikani(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, x.getAmecoffee()) == false) {
            return null;
        }
        if (chekWater(x, x.getAmewater()) == false) {
            return null;
        }
        if (chekBeansTank(x, x.getAmecoffee()) == false){
            return null;
        }
        setBeanstank(getBeanstank() - x.getAmecoffee());
        x.setCoffee(x.getCoffee() + x.getAmecoffee());
        x.setCoffee(x.getCoffee() - x.getAmecoffee());
        x.setWater(x.getWater() - x.getAmewater());
        x.setTank(x.getTank() + x.getAmecoffee());
        return getAmericano();
    }

    public Latte cookLatte(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, getLattecoffee()) == false) {
            return null;
        }
        if (chekWater(x, getLattewater()) == false) {
            return null;
        }
        if (chekBeansTank(x, getLattecoffee()) == false){
            return null;
        }
        System.out.println("How much milk to add to coffee??");
        Scanner scn = new Scanner(System.in);
        int milkuse = scn.nextInt();
        setMilkonecup(milkuse);
        if (chekMilk(x, getMilkonecup()) == false){
            return null;
        }
        setBeanstank(getBeanstank() - x.getAmecoffee());
        x.setCoffee(x.getCoffee() + x.getAmecoffee());
        x.setCoffee(x.getCoffee() - x.getExpcoffee());
        x.setWater(x.getWater() - x.getExpwater());
        x.setTank(x.getTank() + x.getExpcoffee());
        setMilk(getMilk() - getMilkonecup());
        return getLatte();
    }

    public boolean chekTank(Machine x, int coffeeOneCup) {
        if (x.getTank() + coffeeOneCup > x.getMaxtank()) {
            showMessageTank();
            System.out.println(x.getTank() + coffeeOneCup + x.getMaxtank());
            return false;
        }
        return true;
    }

    public boolean chekWater(Machine x, int waterOneCup) {
        if (waterOneCup > x.getWater()) {
            showMessageWater();
            return false;
        }
        return true;
    }

    public boolean chekMilk(Machine x, int milkOneCup) {
        if (milk < milkonecup) {
            showMessageMilk();
            return false;
        }
        return true;
    }

    public boolean chekBeansTank(Machine x, int coffeeOneCup) {
        if (coffeeOneCup > getBeanstank()) {
            showMessageBeansCoffee();
            return false;
        }
        return true;
    }
}
