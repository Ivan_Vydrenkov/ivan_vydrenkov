//Написать методы работы с квадратными матрицами (матрицы представить в виде двухмерных массивов).
//        Должны присутствовать методы:
//        +создания единичной (диагональной) матрицы;
//        +создания нулевой матрицы;
//        +сложение матриц;
//        +умножения матриц;
//        +умножение матрицы на скаляр;
//        +определение детерминанта матрицы;
//        +вывод матрицы на консоль.

public class Task01 {
    public static int[][] getArray(int x) {
        int y = x;
        int[][] arr = new int[x][y];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == j) {
                    arr[i][j] = 1;
                } else {
                    arr[i][j] = 0;
                }
            }
        }
        return arr;


    }

    public static int[][] get0Array(int x) {
        int y = x;
        int[][] arr = new int[x][y];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = 0;
            }
        }
        return arr;
    }

    public static int[][] randomMatrix(int x) {
        int y = x;
        int[][] arr = new int[x][y];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 20 + 1);
            }
        }
        return arr;
    }

    public static int[][] getMatrixSum(int[][] first, int[][] second, int x) {
        int[][] arr = new int[x][x];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = first[i][j] + second[i][j];
            }
        }
        return arr;
    }

    public static int[][] getMatrixMp(int[][] first, int[][] second, int x) {
        int[][] arr = new int[x][x];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = first[i][j] * second[i][j];
            }
        }
        return arr;
    }

    public static int[][] getMatrixMpSkal(int[][] first, int x) {
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first[i].length; j++) {
                first[i][j] = first[i][j] * x;
            }
        }
        return first;
    }

    public static int getDeterminant(int [][] matrix){
        int determinant,x,y,z;
        x=(matrix[0][0] * (matrix[1][1] * matrix[2][2]
                - matrix[1][2] * matrix[2][1]));
        y=(matrix[0][1] * (matrix[1][0] * matrix[2][2]
                - matrix[1][2] * matrix[2][0]));
        z=(matrix[0][2] * (matrix[1][0] * matrix[2][1]
                - matrix[1][1] * matrix[2][0]));
        determinant = x - y + z;
        return determinant;
    }


    public static void printArray(int[][] arr) {
        for (int[] anArr : arr) {
            for (int anAnArr : anArr) {
                System.out.print(anAnArr + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[][] array1 = getArray(5);
        printArray(array1);
        int[][] array2 = get0Array(5);
        printArray(array2);
        int i = 3;
        int[][] matrix = randomMatrix(i);
        printArray(matrix);
        int[][] matrix2 = randomMatrix(i);
        printArray(matrix2);
        int[][] matrixSum = getMatrixSum(matrix, matrix2, i);
        printArray(matrixSum);
        int[][] matrixmultiply = getMatrixMp(matrix, matrix2, i);
        printArray(matrixmultiply);
        int[][] matrixMpSkal = getMatrixMpSkal(matrix, 2);
        printArray(matrixMpSkal);
        System.out.println(getDeterminant(matrix));

    }

}
