public class MainString {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    private char[] arr;
    private int length;

    public char[] getArr() {
        return arr;
    }

    public void setArr(char[] arr) {
        this.arr = arr;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public MainString() {
    }

    public MainString(String str) {
        this.str = str;
        char[] arr = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            arr[i] = str.charAt(i);
        }
        this.arr = arr;
    }

    public MainString(char x) {
        char[] arr = new char[length];
        arr[0] = x;
        this.arr = arr;
        String str = new String(arr);
        this.str = str;
    }

    public static int lengthString(MainString x) {
        return x.str.length();
    }

    public static String nullString(MainString x) {
        x.str = "";
        return x.str;
    }

public static void charInString (MainString y, char x){
    for (int i = 0; i<y.arr.length; i++){
        if (y.arr[i]==x){
            System.out.print(i + " ");
        }
    }
}
}
