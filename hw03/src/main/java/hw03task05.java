import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class hw03task05 {
    public static void main(String[] args) throws IOException {
        System.out.print("Enter your date of birth (in format XX XX XXXX)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String d, m, y;
        String[] data = reader.readLine().split(" ");
        d = data[0];
        m = data[1];
        y = data[2];


        int day = Integer.parseInt(d);
        int month = Integer.parseInt(m);
        int year = Integer.parseInt(y);

        double y1 = (double) year;
        double r = y1 / 12.0;
        int remainder = (int) r;
        double r1 = (double) remainder;
        double r2 = r - r1;

        double yrat = 2008;
        double rrat = yrat / 12.0;
        int remainderrat = (int) rrat;
        double r1rat = (double) remainderrat;
        double r2rat = rrat - r1rat;

        double ybull = 2009;
        double rbull = ybull / 12.0;
        int remainderbull = (int) rbull;
        double r1bull = (double) remainderbull;
        double r2bull = rbull - r1bull;

        double ytiger = 2010;
        double rtiger = ytiger / 12.0;
        int remaindertiger = (int) rtiger;
        double r1tiger = (double) remaindertiger;
        double r2tiger = rtiger - r1tiger;

        double yrabbit = 2011;
        double rrabbit = yrabbit / 12.0;
        int remainderrabbit = (int) rrabbit;
        double r1rabbit = (double) remainderrabbit;
        double r2rabbit = rrabbit - r1rabbit;

        double ydragon = 2012;
        double rdragon = ydragon / 12.0;
        int remainderdragon = (int) rdragon;
        double r1dragon = (double) remainderdragon;
        double r2dragon = rdragon - r1dragon;

        double ysnake = 2013;
        double rsnake = ysnake / 12.0;
        int remaindersnake = (int) rsnake;
        double r1snake = (double) remaindersnake;
        double r2snake = rsnake - r1snake;

        double yhorse = 2014;
        double rhorse = yhorse / 12.0;
        int remainderhorse = (int) rhorse;
        double r1horse = (double) remainderhorse;
        double r2horse = rhorse - r1horse;

        double ygoat = 2015;
        double rgoat = ygoat / 12.0;
        int remaindergoat = (int) rgoat;
        double r1goat = (double) remaindergoat;
        double r2goat = rgoat - r1goat;

        double ymonkey = 2016;
        double rmonkey = ymonkey / 12.0;
        int remaindermonkey = (int) rmonkey;
        double r1monkey = (double) remaindermonkey;
        double r2monkey = rmonkey - r1monkey;

        double ycock = 2017;
        double rcock = ycock / 12.0;
        int remaindercock = (int) rcock;
        double r1cock = (double) remaindercock;
        double r2cock = rcock - r1cock;

        double ydog = 2018;
        double rdog = ydog / 12.0;
        int remainderdog = (int) rdog;
        double r1dog = (double) remainderdog;
        double r2dog = rdog - r1dog;

        double ypig = 2019;
        double rpig = ypig / 12.0;
        int remainderpig = (int) rpig;
        double r1pig = (double) remainderpig;
        double r2pig = rpig - r1pig;

        if (r2 == r2rat)
            System.out.println("Year of the rat.");
        else if (r2 == r2bull)
            System.out.println("Year of the bull.");
        else if (r2 == r2tiger)
            System.out.println("Year of the tiger.");
        else if (r2 == r2rabbit)
            System.out.println("Year of the rabbit.");
        else if (r2 == r2dragon)
            System.out.println("Year of the dragon.");
        else if (r2 == r2snake)
            System.out.println("Year of the snake.");
        else if (r2 == r2horse)
            System.out.println("Year of the Horse.");
        else if (r2 == r2goat)
            System.out.println("Year of the goat.");
        else if (r2 == r2monkey)
            System.out.println("Year of the monkey.");
        else if (r2 == r2cock)
            System.out.println("Year of the cock.");
        else if (r2 == r2dog)
            System.out.println("Year of the dog.");
        else if (r2 == r2pig)
            System.out.println("Year of the Pig.");
        else System.out.println("Year not correct.");

        if (month == 1 && day <= 19 || month == 12 && day > 21)
            System.out.println("Capricorn.");
        else if (month == 1 && day > 19 || month == 2 && day <= 19)
            System.out.println("Aquarius.");
        else if (month == 2 && day > 19 || month == 3 && day <= 20)
            System.out.println("Fish.");
        else if (month == 3 && day > 20 || month == 4 && day <= 20)
            System.out.println("Aries.");
        else if (month == 4 && day > 20 || month == 5 && day <= 20)
            System.out.println("Taurus.");
        else if (month == 5 && day > 20 || month == 6 && day <= 20)
            System.out.println("Twins.");
        else if (month == 6 && day > 20 || month == 7 && day <= 22)
            System.out.println("Crayfish.");
        else if (month == 7 && day > 22 || month == 8 && day <= 22)
            System.out.println("Lion.");
        else if (month == 8 && day > 22 || month == 9 && day <= 22)
            System.out.println("Virgo.");
        else if (month == 9 && day > 22 || month == 10 && day <= 22)
            System.out.println("Libra.");
        else if (month == 10 && day > 22 || month == 11 && day <= 22)
            System.out.println("Scorpio.");
        else if (month == 11 && day > 22 || month == 12 && day <= 21)
            System.out.println("Sagittarius.");
    }
}
