public class Task03 {
    public static void main(String[] args) {

        for (double i = 10; i <= 1000000; i++) {
            double a;
            if (i == 1000000) {
                double x1 = i % 10;
                double x2 = (i % 100 - x1) / 10;
                double x3 = (i % 1000 - i % 100) / 100;
                double x4 = (i % 10000 - i % 1000) / 1000;
                double x5 = (i % 100000 - i % 10000) / 10000;
                double x6 = (i % 1000000 - i % 100000) / 100000;
                double x7 = (i - i % 1000000) / 1000000;
                a = Math.pow(x1, 7) + Math.pow(x2, 7) + Math.pow(x3, 7) + Math.pow(x4, 7) + Math.pow(x5, 7) + Math.pow(x6, 7) + Math.pow(x7, 7);
                if (a == i) {
                    System.out.println(a);
                }
            } else if (i >= 100000 && i < 1000000) {
                double x1 = i % 10;
                double x2 = (i % 100 - x1) / 10;
                double x3 = (i % 1000 - i % 100) / 100;
                double x4 = (i % 10000 - i % 1000) / 1000;
                double x5 = (i % 100000 - i % 10000) / 10000;
                double x6 = (i - i % 100000) / 100000;
                a = Math.pow(x1, 6) + Math.pow(x2, 6) + Math.pow(x3, 6) + Math.pow(x4, 6) + Math.pow(x5, 6) + Math.pow(x6, 6);
                if (a == i) {
                    System.out.println(a);
                }
            } else if (i >= 10000 && i < 100000) {
                double x1 = i % 10;
                double x2 = (i % 100 - x1) / 10;
                double x3 = (i % 1000 - i % 100) / 100;
                double x4 = (i % 10000 - i % 1000) / 1000;
                double x5 = (i - i % 10000) / 10000;
                a = Math.pow(x1, 5) + Math.pow(x2, 5) + Math.pow(x3, 5) + Math.pow(x4, 5) + Math.pow(x5, 5);
                if (a == i) {
                    System.out.println(a);
                }
            } else if (i >= 1000 && i < 10000) {
                double x1 = i % 10;
                double x2 = (i % 100 - x1) / 10;
                double x3 = (i % 1000 - i % 100) / 100;
                double x4 = (i - i % 1000) / 1000;
                a = Math.pow(x1, 4) + Math.pow(x2, 4) + Math.pow(x3, 4) + Math.pow(x4, 4);
                if (a == i) {
                    System.out.println(a);
                }
            } else if (i >= 100 && i < 1000) {
                double x1 = i % 10;
                double x2 = (i % 100 - x1) / 10;
                double x3 = (i - i % 100) / 100;
                a = Math.pow(x1, 3) + Math.pow(x2, 3) + Math.pow(x3, 3);
                if (a == i) {
                    System.out.println(a);
                }
            } else if (i >= 10 && i < 100) {
                double x1 = i % 10;
                double x2 = (i - i % 10) / 10;
                a = Math.pow(x1, 2) + Math.pow(x2, 2);
                if (a == i) {
                    System.out.println(a);
                }
            }
        }
    }
}
