public class hw03task04 {
    public static void main(String[] args) {
        double x = 1;
        double y = -10;
        if (x == 0 || y == 0)
            System.out.println("Zero");
        else if (x >= -10 && x < 0 && y <= 10 && y > 0)
            System.out.println("I");
        else if (x <= 10 && x > 0 && y <= 10 && y > 0)
            System.out.println("II");
        else if (x >= -10 && x < 0 && y >= -10 && y < 0)
            System.out.println("III");
        else if (x <= 10 && x > 0 && y >= -10 && y < 0)
            System.out.println("IV");
        else System.out.println("Error");

    }
}
