import java.util.Scanner;

public class hw02task09
{
    public static void main(String[] args) {

        double a = 3.0;
        int z = (int) a;
        double d = a - z;
        if (d > 0) System.out.println("У числа " + a +  " есть вещественная часть.");
        else System.out.println("У числа " + a + " нет вещественной части.");
    }
}
