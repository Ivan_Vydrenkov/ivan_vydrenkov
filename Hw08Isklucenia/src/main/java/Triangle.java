public class Triangle {
    private int firstTriangleSide;
    private int secondTriangleSide;
    private int thirdTriangleSide;
    public Triangle (int firstTriangleSide, int secondTriangleSide, int thirdTriangleSide) throws NotPossibleToCreate {
        if ((firstTriangleSide<secondTriangleSide+thirdTriangleSide)&&(secondTriangleSide<firstTriangleSide+thirdTriangleSide)&&(thirdTriangleSide<firstTriangleSide+secondTriangleSide)||firstTriangleSide<=0||secondTriangleSide<=0||thirdTriangleSide<=0){
throw new NotPossibleToCreate("Not possible to create triangle!");
        }
        this.firstTriangleSide=firstTriangleSide;
        this.secondTriangleSide=secondTriangleSide;
        this.thirdTriangleSide=thirdTriangleSide;
    }
}
