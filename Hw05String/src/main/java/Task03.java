import java.util.Scanner;
import java.util.StringTokenizer;

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();

        StringTokenizer str = new StringTokenizer(str1);
        double words = str.countTokens();

        String str2 = str1.replace(" ", "");
        str2 = str2.replace(".", "");
        str2 = str2.replace(",", "");

        double i = str2.length();
        System.out.println(Math.round(i / words));
    }
}
