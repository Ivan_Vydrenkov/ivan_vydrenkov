import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(String.format("%32s", Integer.toBinaryString(num)).replace(' ', '0'));

        System.out.println("Enter the number of bits:");
        int bit = scan.nextInt();

        int maska = -1 >>> bit;
        int newNumber = num & maska;
        System.out.println("New number:" + "\n" + String.format("%32s", Integer.toBinaryString(newNumber)).replace(' ', '0'));
    }
}
