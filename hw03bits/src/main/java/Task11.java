import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(Integer.toBinaryString(num));
        int sum = 0;

        while (num != 0) {
            sum += (num & 1);
            num >>>= 1;
        }
        System.out.println(sum);
    }
}
