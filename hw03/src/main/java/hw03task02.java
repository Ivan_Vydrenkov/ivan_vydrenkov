import java.util.Scanner;

public class hw03task02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter time:");
        int time = in.nextInt();
        if (time >= 4 && time <= 9)
            System.out.println("Good morning.");
        else if (time > 9 && time < 12) System.out.println("Good day.");
        else if (time == 12) System.out.println("Noon.");
        else if (time > 12 && time <= 16) System.out.println("Good day.");
        else if (time > 16 && time <= 22) System.out.println("Good evening.");
        else if (time > 22 && time <= 24) System.out.println("Good night.");
        else if (time >= 0 && time < 4) System.out.println("Good night.");
        else System.out.println("Time not correct.");
        in.close();
    }
}
