import java.util.Scanner;

//      Создать класс Money (Деньги) для работы с денежными суммами. Число должно быть представлено двумя полями:
//       типа long – для гривен;
//       типа byte – для копеек.
//      Реализовать вывод значения на экран, при этом дробная часть должна быть отделена от целой части запятой. Реали-
//      зовать сложение, вычитание, деление сумм, деление суммы на дробное число, умножение на дробное число и операции сравнения.

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of rubles:");
        long rub = scanner.nextLong();
        System.out.println("Enter the number of cents:");
        byte cents = scanner.nextByte();

        Money money = new Money(rub, cents);

        System.out.println("Enter the number of rubles:");
        long rub2 = scanner.nextLong();
        System.out.println("Enter the number of cents:");
        byte cents2 = scanner.nextByte();

        Money money2 = new Money(rub2, cents2);

        Money.toString(money, money2);
        Money.sumMoney(money, money2);
        Money.subtractionMoney(money, money2);
        Money.divisionMoney(money, money2);
        Money.divisionOnShareMoney(money, money2);
        Money.comparisonMoney(money, money2);

    }
}
