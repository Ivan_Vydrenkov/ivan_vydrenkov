import java.util.Arrays;
import java.util.Random;


//Написать и протестировать перегруженный метод, выводящий на экран:
//        +одномерный массив типа int;
//        +одномерный массив типа String;
//        +двухмерный массив типа int;
//        +двухмерный массив типа float.

public class Task02 {
    public static void toString(int[] numbers) {
        String content = Arrays.toString(numbers);
        System.out.println(content);
    }

    public static void toString(String [] arr) {
        String content = Arrays.toString(arr);
        System.out.println(content);
    }

    public static void toString(int [] [] arr) {
        Random rnd = new Random();
        for (int i=0;i < arr.length;i++) {
            for (int j=0;j < arr[i].length;j++) {
                arr[i][j]=rnd.nextInt(10) + 1;
            }
        }
        for (int i=0;i < arr.length;i++,System.out.println()) {
            for (int j=0;j < arr[i].length;j++) {
                System.out.print(arr[i][j]+" ");
            }
        }
    }


    public static void toString(float [] [] arr) {
        Random rnd = new Random();
        for (int i=0;i < arr.length;i++) {
            for (int j=0;j < arr[i].length;j++) {
                arr[i][j]=rnd.nextFloat() + 1;
            }
        }
        for (int i=0;i < arr.length;i++,System.out.println()) {
            for (int j=0;j < arr[i].length;j++) {
                System.out.print(arr[i][j]+" ");
            }
        }
    }



    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5};
        toString(arr1);

        String[] arr2 = {"one", "two", "three", "four", "five"};
        toString(arr2);

        int[][] arr3 = new int[3][3];
        toString(arr3);

        float[][] arr4 = new float[3][3];
        toString(arr4);
    }
}
