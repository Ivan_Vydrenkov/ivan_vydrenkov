public class Сalculator {

    public static void main(String[] args) {
        Hourly sam = new Hourly("Sam Smith", true, "Hourly", false, 400, 10);
        Piecework bob = new Piecework("Bob Bond", false, "Piecework", true, 5, 500);
        Rate jack = new Rate("Jack Wade", false, "Rate", false, 21, 40);
        Employee[] arr = new Employee[3];
        arr[0] = sam;
        arr[1] = bob;
        arr[2] = jack;
        task01(arr);
        task02(arr);
        task03(arr);
        task04(arr);
        task05(arr);
        task06(arr);
    }

    public static int sum(Employee[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i].personalEarning();
        }
        return sum;
    }

    public static int toPay(Employee x) {
        return (x.personalEarning() / 100) * ((100 - x.getTax()));
    }

    public static int toPayChildren(Employee x) {
        if (x.isChildren()) {
            return (x.personalEarning() / 100) * ((100 - x.getTax()));
        } else {
            return (x.personalEarning() / 100) * ((100 - x.getTax()) - 5);
        }
    }

    public static int toSumPay(Employee[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += (arr[i].personalEarning() / 100) * ((100 - arr[i].getTax()));
        }
        return sum;
    }

    public static int toSumPayChildren(Employee[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].isChildren()) {
                sum += (arr[i].personalEarning() / 100) * ((100 - arr[i].getTax()));
            } else {
                sum += (arr[i].personalEarning() / 100) * ((100 - arr[i].getTax()) - 5);
            }
        }
        return sum;
    }

    public static String toPayHourly(Employee x) {
        if (x.isChildren()) {
            int one = (x.personalEarning() / 100) * (100 - x.getTax()) / 2;
            int two = one / 8;
            return one + "/" + two;
        } else {
            int one = (x.personalEarning() / 100) * (100 - x.getTax() - 5) / 2;
            int two = one / 8;
            return one + "/" + two;
        }

    }

    public static int getPrize(Employee x) {
        if (x instanceof Hourly) {
            return ((Hourly) x).getTime();
        } else if (x instanceof Piecework) {
            return ((Piecework) x).getWorks() * 100;
        } else {
            return ((Rate) x).getDays() * 8;
        }
    }

    public static String toPayHourlyPrize(Employee x) {
        if (x.isChildren()) {
            int one = ((x.personalEarning() + 500) / 100) * (100 - x.getTax()) / 2;
            int two = one / 8;
            return one + "/" + two;
        } else {
            int one = ((x.personalEarning() + 500) / 100) * (100 - x.getTax() - 5) / 2;
            int two = one / 8;
            return one + "/" + two;
        }

    }


//Task01 Разработать и протестировать набор классов для решения задачи подсчета зароботной платы сотрудников некоторой фирмы.

    public static void task01(Employee[] arr) {
        System.out.println("FIO         |Payment   |Earning");
        System.out.println("-------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");
            System.out.print(arr[i].getPayment());
            int p = 10 - arr[i].getPayment().length();
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");
            System.out.println(arr[i].personalEarning());
        }
        System.out.println("-------------------------------");
        System.out.println("Total                    " + sum(arr));
    }

    //Task02 Для предыдущего задания вывести отчет с учетом налогов.
    //Для сотрудников, которые на ставке и почасовой оплате – налог 20%.
    //Для сотрудников со сдельной оплатой труда – 15%.

    public static void task02(Employee[] arr) {
        System.out.println("FIO         |Tax,%  |Earning |To pay");
        System.out.println("------------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].getTax() + "%");
            int p = 4;
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].personalEarning());
            String str = "" + arr[i].personalEarning();
            int e = 8 - str.length();
            for (; e > 0; e--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.println(toPay(arr[i]));
        }
        System.out.println("-----------------------------------");
        System.out.println("Total                         " + toSumPay(arr));
    }

//  Task03  На основе предыдущего задания сделать новый отчет таким образом, что для сотрудников, у которых
//  нет детей, ставка налога выше на 5%.

    public static void task03(Employee[] arr) {
        System.out.println("FIO         |Tax,%  |Earning |To pay");
        System.out.println("------------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");

            if (arr[i].isChildren()) {
                System.out.print(arr[i].getTax() + "%");
            } else {
                System.out.print((arr[i].getTax() + 5) + "%");
            }
            int p = 4;
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].personalEarning());
            String str = "" + arr[i].personalEarning();
            int e = 8 - str.length();
            for (; e > 0; e--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.println(toPayChildren(arr[i]));
        }
        System.out.println("-----------------------------------");
        System.out.println("Total                         " + toSumPayChildren(arr));
    }

//    Task04 На основе предыдущего задания переделать отчет, с учетом того, что сотрудники
//    с почасовой оплатой, половину зарплаты получают в валюте (тугриках), по курсу на день
//    начисления заработной платы.

    public static void task04(Employee[] arr) {
        System.out.println("            |       |        |To pay");
        System.out.println("FIO         |Tax,%  |Earning |(rub/USD");
        System.out.println("            |       |        | 1/8)");
        System.out.println("---------------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].getTax() + "%");
            int p = 4;
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].personalEarning());
            String str = "" + arr[i].personalEarning();
            int e = 8 - str.length();
            for (; e > 0; e--) {
                System.out.print(" ");
            }
            System.out.print("|");

            if (arr[i] instanceof Hourly) {
                System.out.println(toPayHourly(arr[i]));
            } else {
                System.out.println(toPay(arr[i]));
            }
        }
        System.out.println("---------------------------------------");
    }

//    Task05 Фирма переводит часть сотрудников в офшорную зону. Сотрудники, находящиеся в
//    офшоре, не платят налогов. Создать новый отчет с учетом данного нововведения.

    public static void task05(Employee[] arr) {
        System.out.println("            |       |        |To pay");
        System.out.println("FIO         |Tax,%  |Earning |(rub/USD");
        System.out.println("            |       |        | 1/8)");
        System.out.println("---------------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");

            if (arr[i].isOff()) {
                System.out.print("0% ");
            } else {
                System.out.print(arr[i].getTax() + "%");
            }
            int p = 4;
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");

            System.out.print(arr[i].personalEarning());
            String str = "" + arr[i].personalEarning();
            int e = 8 - str.length();
            for (; e > 0; e--) {
                System.out.print(" ");
            }
            System.out.print("|");
            if (arr[i].isOff()) {
                System.out.println(arr[i].personalEarning());
            } else if (arr[i] instanceof Hourly) {
                System.out.println(toPayHourly(arr[i]));
            } else {
                System.out.println(toPay(arr[i]));
            }
        }
        System.out.println("---------------------------------------");
    }

//    Task06 Фирма вводит премии сотрудникам, которые работали больше 200 часов в месяц,
//    но не находятся в офшоре. Премии должны суммироваться в основную зарплату. Создать
//    новый отчет с учетом изменений.

    public static void task06(Employee[] arr) {
        System.out.println("            |       |               |To pay");
        System.out.println("FIO         |Tax,%  |Earning        |(rub/USD");
        System.out.println("            |       |               | 1/8)");
        System.out.println("---------------------------------------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i].getFio());
            int f = 12 - arr[i].getFio().length();
            for (; f > 0; f--) {
                System.out.print(" ");
            }
            System.out.print("|");


            if (arr[i].isOff()) {
                System.out.print("0% ");
            } else {
                System.out.print(arr[i].getTax() + "%");
            }
            int p = 4;
            for (; p > 0; p--) {
                System.out.print(" ");
            }
            System.out.print("|");


            int earning = arr[i].personalEarning();
            String str;
            if (getPrize(arr[i]) > 200) {
                earning += 500;
                str = earning + "(500 bonus)";
                System.out.print(str);
            } else {
                System.out.print(arr[i].personalEarning());
                str = "" + arr[i].personalEarning();
            }
            int e = 15 - str.length();
            for (; e > 0; e--) {
                System.out.print(" ");
            }
            System.out.print("|");

            int topay = 0;
            if (arr[i].isOff()) {
                topay = arr[i].personalEarning();
                if (getPrize(arr[i]) > 200) {
                    topay += 500;
                }
                System.out.println(topay);
            } else if (arr[i] instanceof Hourly) {
                if (getPrize(arr[i]) > 200) {
                    System.out.println(toPayHourlyPrize(arr[i]));
                } else {
                    System.out.println(toPayHourly(arr[i]));
                }
            } else {
                topay = toPay(arr[i]);
                if (getPrize(arr[i]) > 200) {
                    topay += 500;
                }
                System.out.println(topay);
            }
        }
        System.out.println("-------------------------------------------");
    }
}
