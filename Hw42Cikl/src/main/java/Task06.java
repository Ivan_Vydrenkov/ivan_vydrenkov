import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        String str1 = "***";
        String str2 = "  *";
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        for (; i > 0; i--) {
            System.out.println(str1);
            System.out.println(str2);
            str1 = "  " + str1;
            str2 = "  " + str2;
        }
    }
}
