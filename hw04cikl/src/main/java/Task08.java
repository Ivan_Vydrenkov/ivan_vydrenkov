public class Task08 {
    public static void main(String[] args) {
        int sum = 0;
        int hour1;
        int hour2;
        int min1;
        int min2;
        for (int hour = 0; hour <= 23; hour++) {
            for (int min = 0; min <= 59; min++) {
                hour1 = (hour - hour % 10) / 10;
                hour2 = hour % 10;
                min1 = (min - min % 10) / 10;
                min2 = min % 10;
                if (min1 == hour2 && min2 == hour1) {
                    System.out.println(hour1 + "" + hour2 + ":" + min1 + "" + min2);
                    sum++;
                }
            }
        }
        System.out.println(sum);
    }
}
