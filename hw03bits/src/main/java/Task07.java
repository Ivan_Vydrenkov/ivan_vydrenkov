import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(String.format("%32s", Integer.toBinaryString(num)).replace(' ', '0'));

        System.out.println("Enter bit number:");
        int bit = scan.nextInt();
        System.out.println((num & (1 << bit)) > 0 ? 1 : 0);
    }
}
