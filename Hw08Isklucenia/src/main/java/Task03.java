//Создать класс, объекты которого будут неизменяемыми. Класс инкапсулирует в себе информацию о треугольнике на
//плоскости (длины каждой из его ребер). Длины сторон задаются в конструкторе. Если по заданным сторонам нельзя
//построить треугольник, в конструктор должно бросаться исключение.

public class Task03 {
    public static void main(String[] args) {
        try {
            Triangle triangle = new Triangle(-12, 34, 12);
        } catch (NotPossibleToCreate ex) {
            System.err.println("Not possible to create triangle!");
        }
    }
}
