import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemAlreadyExistsException;

//Создайте метод, который принимает массив объектов данного класса, и вызывает у каждого объекта метод read.
//Если при исполнении будет брошено исключение FileSystemAlreadyExistsException, поймать исключение и бросить исключение FileNotFoundException.

public class Task06 {
    public static void main(String[] args) throws IOException {
        task06Metod(createArr(10));
    }

    public static XmlReader[] createArr(int x) {
        XmlReader[] arr = new XmlReader[x];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new XmlReader();
        }
        return arr;
    }

    public static void task06Metod(XmlReader[] arr) throws IOException {
        for (int i = 0; i < arr.length; i++) {
            try {
                arr[i].read();
            } catch (FileSystemAlreadyExistsException e) {
                try {
                    throw new FileNotFoundException();
                } catch (FileNotFoundException ex) {
                    System.err.println("FileNotFoundException!");
                }
            }
        }
    }
}
