public class Journal {
    private String book;
    private int ticketnum;
    private int day;
    private int monthe;
    private int numdays;
    private int dayback;
    private int montheback;
    private int days;
    private int lastday;
    private int lastmonth;

    public int getLastday() {
        return lastday;
    }

    public void setLastday(int lastday) {
        this.lastday = lastday;
    }

    public int getLastmonth() {
        return lastmonth;
    }

    public void setLastmonth(int lastmonth) {
        this.lastmonth = lastmonth;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public int getTicketnum() {
        return ticketnum;
    }

    public void setTicketnum(int ticketnum) {
        this.ticketnum = ticketnum;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonthe() {
        return monthe;
    }

    public void setMonthe(int monthe) {
        this.monthe = monthe;
    }

    public int getNumdays() {
        return numdays;
    }

    public void setNumdays(int numdays) {
        this.numdays = numdays;
    }

    public int getDayback() {
        return dayback;
    }

    public void setDayback(int dayback) {
        this.dayback = dayback;
    }

    public int getMontheback() {
        return montheback;
    }

    public void setMontheback(int montheback) {
        this.montheback = montheback;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Journal(String book, Ticket x, int day, int monthe, int days) {
        this.book = book;
        this.ticketnum = x.getNum();
        this.day = day;
        this.monthe = monthe;
        this.days = days;
    }

    public static void checkTicket(Journal x) {
        if (x.day + x.days > 31){
            x.lastday = x.day + x.days -31;
            x.lastmonth = x.monthe +1;
        }
        else {
            x.lastmonth = x.monthe;
            x.lastday = x.day + x.days;
        }

            if (x.lastday <= x.dayback && x.lastmonth <= x.montheback) {
                System.out.println("Student ticket number" + x.ticketnum + " passed the book on time.");
            } else {
                System.out.println("Student ticket number" + x.ticketnum + "didn't hand over the book on time.");
            }
    }


}
