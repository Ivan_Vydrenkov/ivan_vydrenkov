import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(String.format("%32s", Integer.toBinaryString(num)).replace(' ', '0'));
        int metka = 1 << 30;
        int index = 0;
        while (num < metka) {
            metka >>= 1;
            index++;
        }
        int newNumber = num & ~(1 << (30 - index));
        System.out.println("New number:" + "\n" + String.format("%32s", Integer.toBinaryString(newNumber)).replace(' ', '0'));
    }
}
