public class Task10 {
    public static void main(String[] args) {
        int sum = 0;
        for (long i = 2520; i <= 1000000000; i++) {
            for (long divider = 1; divider <= 20; divider++) {
                if (i % divider == 0) {
                    sum++;
                } else sum = 0;
            }
            if (sum == 20) {
                System.out.println(i);
                break;
            }
        }
    }
}
