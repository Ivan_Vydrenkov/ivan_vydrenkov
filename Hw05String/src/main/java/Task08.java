import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String res = "";
        if (str.contains("_")) {
            for (int i = 0; i < str.length(); i++) {
                Character ch = str.charAt(i);
                if (ch == '_') {
                    i++;
                    ch = str.charAt(i);
                    res += Character.toUpperCase(ch);
                } else res += ch;
            }
            System.out.println(res);
        } else
            for (int i = 0; i < str.length(); i++) {
                Character ch = str.charAt(i);
                if (Character.isUpperCase(ch)) {
                    res += "_" + Character.toLowerCase(ch);
                } else
                    res += ch;
            }
        System.out.println(res);
    }
}
