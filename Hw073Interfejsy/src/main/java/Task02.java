public class Task02 {
    public static void main(String[] args) {
//        task02 Машина варит каппучино и латте:
        СappuccinoLatte model02 = new СappuccinoLatte("Model 02", 2000, 500, 500, 800);
        model02.setWater(2000);
        model02.setCoffee(500);
        model02.setMilk(800);
        ButtonOn.onMachine(model02);
        model02.cookCappuccino(model02);
        model02.cookLatte(model02);
    }
}
