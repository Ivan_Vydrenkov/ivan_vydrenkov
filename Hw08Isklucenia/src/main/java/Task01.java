import java.util.Scanner;

//Создать метод, который выводит в консоль результат целочисленного деления числа, введенного с клавиатуры, на
//значения элементов одномерного массива целых чисел, заполненный случайным образом – от -10 до 10. Длина массива
// случайная – от 1 до 10. Обработать все возможные исключительные ситуации в данном методе.

public class Task01 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Insert the number");
        showDivision(createRandomArr(), scn.nextInt());
    }

    public static int[] createRandomArr() {
        int[] array = new int[(int) (Math.random() * 10) + 1];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 21) - 10);
        }
        return array;
    }

    public static void showDivision(int[] arr, int i) throws ArithmeticException {
        for (int x = 0; x < arr.length; x++) {
            try {
                int result = i / arr[x];
                System.out.println(result);
            } catch (ArithmeticException ex) {
                System.err.println("Сan not be divided by zero");
            }
        }
    }
}
