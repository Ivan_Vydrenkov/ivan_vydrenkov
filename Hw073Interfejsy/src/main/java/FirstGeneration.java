public interface FirstGeneration {
    void showMessageWater();
    void showMessageCoffee();
    void showMessageTank();

    Object getExpresso();
    Object getAmericano();

    void clearTank(Machine x);
    void putCoffee(Machine x, int coffee);
    void putWater(Machine x, int water);

    Expresso cookExpresso (Machine x);
    Americano cookAmerikani (Machine x);

    boolean chekTank (Machine x, int i);
    boolean chekWater (Machine x, int i);
    boolean chekCoffee (Machine x, int i);

}
