public class Rate extends Employee {
    private int days;
    private int daypay;
    private int tax = 20;

    @Override
    public int getTax() {
        return tax;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDaypay() {
        return daypay;
    }

    public void setDaypay(int daypay) {
        this.daypay = daypay;
    }

    public Rate(String fio, boolean children, String payment, boolean off, int days, int daypay) {
        super(fio, children, payment, off);
        this.days = days;
        this.daypay = daypay;
    }

    public int personalEarning() {
        return days * daypay;
    }
}
