import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;

import java.util.Scanner;

public class hw03task12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter six digit number: ");
        double num = in.nextDouble();
        double x1 = num % 10;
        double x2 = (num % 100 - x1) / 10;
        double x3 = (num % 1000 - num % 100) / 100;
        double x4 = (num % 10000 - num % 1000) / 1000;
        double x5 = (num % 100000 - num % 10000) / 10000;
        double x6 = (num - num % 100000) / 100000;
        double part1 = x1 + x2 + x3;
        double part2 = x4 + x5 + x6;
        if (part1 == part2)
            System.out.println("Yes.");
        else System.out.println("No.");
    }
}
