import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
//        int length = str.length();
        str = str.toLowerCase();
        if (str.contains("[^a-c ]")) {
            System.out.println("No");
        } else if (str.contains("bb")) {
            System.out.println("No");
        } else if (str.contains("aaa") || str.contains("bbb") || str.contains("ccc")) {
            System.out.println("No");
        } else if (str.contains("ababab") || str.contains("bababa") || str.contains("cacaca") || str.contains("acacac") || str.contains("cbcbcb") || str.contains("bcbcbc")) {
            System.out.println("No");
        } else if (str.contains("abcabcabc") || str.contains("acbacbacb") || str.contains("bacbacbac") || str.contains("bcabcabca") || str.contains("cbacbacba") || str.contains("cabcabcab")) {
            System.out.println("No");
        } else System.out.println("Yes");
    }
}
