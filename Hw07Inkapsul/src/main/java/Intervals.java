public class Intervals {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Intervals(String str) {
        this.str = str;
    }


    public static void toStringIntervals(Intervals one) {
        String[] words = one.str.split("\\s+|,\\s*");
        int[] num = strArrayToIntArray(words);
        int number = 1;
        if (num.length % 2 == 0) {
            for (int i = 0; i < num.length; i++) {
                int x = num[i];
                int y = num[i + 1];
                if (x < y) {
                    System.out.println(number + " interval is " + interval(x, y));
                } else {
                    System.out.println(number + " interval is entered incorrectly.");
                }
                i++;
                number++;
            }
        } else {
            System.out.println("Error.");
        }
    }


    public static int[] strArrayToIntArray(String[] a) {
        int[] b = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            b[i] = Integer.parseInt(a[i]);
        }
        return b;
    }

    public static int interval(int x, int y) {
        int b = y - x;
        return b;
    }

    public static void coincidences(Intervals one) {
        String[] words = one.str.split("\\s+|,\\s*");
        int[] num = strArrayToIntArray(words);
        int number1 = 1;
        if (num.length % 2 == 0) {
            for (int i = 0; i < num.length; i++) {
                int x = num[i];
                int y = num[i + 1];
                for (int q = i + 2; q < num.length; q++) {
                    int number2 = q / 2 + 1;
                    int z = num[q];
                    int v = num[q + 1];
                    if (z <= x && x <= v) {
                        System.out.println("Interval " + number1 + " has matches with intarval " + number2 + ".");
                    } else if (z <= y && y <= v) {
                        System.out.println("Interval " + number1 + " has matches with intarval " + number2 + ".");
                    } else if (x <= z && z <= y) {
                        System.out.println("Interval " + number1 + " has matches with intarval " + number2 + ".");
                    } else if (x <= v && v <= y) {
                        System.out.println("Interval " + number1 + " has matches with intarval " + number2 + ".");
                    }
                    q++;
                }
                i++;
                number1++;
            }
        } else {
            System.out.println("Error.");
        }
    }
}
