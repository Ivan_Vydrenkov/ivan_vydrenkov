public abstract class Employee {
    private String fio;
    private boolean children;
    private String payment;
    private int earning;
    private int tax;
    private boolean off;

    public boolean isOff() {
        return off;
    }

    public void setOff(boolean off) {
        this.off = off;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getEarning() {
        return earning;
    }

    public void setEarning(int earning) {
        this.earning = earning;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public boolean isChildren() {
        return children;
    }

    public void setChildren(boolean children) {
        this.children = children;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Employee(String fio, boolean children, String payment, boolean off) {
        this.fio = fio;
        this.children = children;
        this.payment = payment;
        this.off=off;
    }

    public abstract int personalEarning();
}
