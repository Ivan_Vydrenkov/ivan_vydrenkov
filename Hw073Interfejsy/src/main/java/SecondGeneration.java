public interface SecondGeneration {
    void showMessageWater();
    void showMessageCoffee();
    void showMessageTank();
    void showMessageMilk();

    Object getExpresso();
    Object getAmericano();
    Object getCappuccino();
    Object getLatte();

    void clearTank(Machine x);
    void putCoffee(Machine x, int coffee);
    void putWater(Machine x, int water);
    void putMilk(Machine x, int milk);

    Expresso cookExpresso (Machine x);
    Americano cookAmerikani (Machine x);
    Cappuccino cookCappuccino (Machine x);
    Latte cookLatte (Machine x);

    boolean chekTank (Machine x, int coffeeOneCup);
    boolean chekWater (Machine x, int waterOneCup);
    boolean chekCoffee (Machine x, int coffeeOneCup);
    boolean chekMilk (Machine x, int milkOneCup);
}
