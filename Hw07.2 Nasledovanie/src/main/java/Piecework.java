public class Piecework extends Employee {
    private int works;
    private int workcost;

    @Override
    public int getTax() {
        return tax;
    }

    private int tax = 15;


    public int getWorks() {
        return works;
    }

    public void setWorks(int works) {
        this.works = works;
    }

    public int getWorkcost() {
        return workcost;
    }

    public void setWorkcost(int workcost) {
        this.workcost = workcost;
    }

    public Piecework(String fio, boolean children, String payment, boolean off, int works, int workcost) {
        super(fio, children, payment, off);
        this.works = works;
        this.workcost = workcost;
    }

    public int personalEarning() {
        return works * workcost;
    }
}
