public class Task04 {

//    Описать базовый класс MainString (Строка). Обязательные поля класса:
//       +массив символов;
//       +значение типа int хранит длину строки в символах.
//    Реализовать обязательные методы следующего назначения:
//         +конструктор без параметров;
//         +конструктор, принимающий в качестве параметра строковый литерал;
//         +конструктор, принимающий в качестве параметра символ;
//         +метод получения длины строки;
//         +метод очистки строки (делает строку пустой);
//         +метод поиска символа в строке.

    public static void main(String[] args) {
        MainString mainstr = new MainString("ajfdahe irjgij");
        System.out.println(MainString.lengthString(mainstr));
        System.out.println(MainString.nullString(mainstr));
        MainString.charInString(mainstr,'a');
    }
}
