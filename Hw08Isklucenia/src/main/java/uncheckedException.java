public class uncheckedException extends RuntimeException {
    public uncheckedException() {
    }

    public uncheckedException(String message) {
        super(message);
    }

    public uncheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public uncheckedException(Throwable cause) {
        super(cause);
    }

    public uncheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
