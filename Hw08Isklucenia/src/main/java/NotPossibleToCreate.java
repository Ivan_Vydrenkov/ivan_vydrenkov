public class NotPossibleToCreate extends Exception {
    public NotPossibleToCreate() {
    }

    public NotPossibleToCreate(String message) {
        super(message);
    }

    public NotPossibleToCreate(String message, Throwable cause) {
        super(message, cause);
    }

    public NotPossibleToCreate(Throwable cause) {
        super(cause);
    }

    public NotPossibleToCreate(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
