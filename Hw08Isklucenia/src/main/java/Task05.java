import java.io.IOException;

//Дан класс Runner. Напишите код, который создает объекты данного класса и вызывает метод halt. В случае, если в методе было брошено
//RuntimeException, вывести в консоль halt; в противном случае – пробросить исключение наверх.

public class Task05 {
    public static void main(String[] args)throws IOException {
        Runner ranner = new Runner();
        try {
            ranner.halt();
        }
        catch (RuntimeException ex){
            System.err.println("halt");
        }
    }
}
