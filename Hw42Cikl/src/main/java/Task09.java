import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        int t = scanner.nextInt();
        for (; t > 0; t--) {
            int h = i;
                int q = 1;

                for (; h > 0; h--) {
                    int d = h+(t-1);
                    for (; d > 0; d--) {
                        System.out.print(" ");
                    }
                    for (int b = 1; b < q; b++) {
                        System.out.print("*");
                        System.out.print("*");
                    }
                    q++;
                    System.out.println("*");
                }
                i++;
            }
        }
//    }
}
