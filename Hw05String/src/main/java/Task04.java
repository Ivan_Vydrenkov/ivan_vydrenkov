import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String[] wordArray = text.split("[\\s,.:!?]+");
        Pattern pattern = Pattern.compile("^[AEIOUYaeiouy].*[bcdfghjklmnpqrstvwxyz]$");
        for (String word : wordArray) {
            Matcher matcher = pattern.matcher(word);
            if (matcher.find()) {
                System.out.println(word);
            }
        }
    }
}

