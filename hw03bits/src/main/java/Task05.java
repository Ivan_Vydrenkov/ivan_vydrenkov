import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(Integer.toBinaryString(num));

        System.out.println("Enter bit number:");
        int bit = scan.nextInt();

        int num2 = num & ~(1 << bit);
        System.out.println("New number:" + "\n" + Integer.toBinaryString(num2));
    }
}
