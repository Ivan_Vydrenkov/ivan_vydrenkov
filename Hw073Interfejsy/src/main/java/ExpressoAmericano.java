public class ExpressoAmericano extends Machine implements FirstGeneration {
    public void showMessageWater() {
        System.out.println("Not enough water.");
    }

    public void showMessageCoffee() {
        System.out.println("Not enough coffee.");
    }

    public void showMessageTank() {
        System.out.println("The tank is full.");
    }

    public Expresso getExpresso() {
        System.out.println("Take your Expresso.");
        return new Expresso();
    }

    public Americano getAmericano() {
        System.out.println("Take your Americano.");
        return new Americano();
    }

    public void clearTank(Machine x) {
        setTank(0);
    }

    public void putCoffee(Machine x, int coffee) {
        setCoffee(coffee);
    }

    public void putWater(Machine x, int water) {
        setWater(water);
    }

    public Expresso cookExpresso(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, x.getExpcoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, x.getExpcoffee()) == false) {
            return null;
        }
        if (chekWater(x, x.getExpwater()) == false) {
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getExpcoffee());
        x.setWater(x.getWater() - x.getExpwater());
        x.setTank(x.getTank() + x.getExpcoffee());
        return getExpresso();
    }

    public Americano cookAmerikani(Machine x) {
        if (x.getOnOff() == false) {
            return null;
        }
        if (chekTank(x, x.getAmecoffee()) == false) {
            return null;
        }
        if (chekCoffee(x, x.getAmecoffee()) == false) {
            return null;
        }
        if (chekWater(x, x.getAmewater()) == false) {
            return null;
        }
        x.setCoffee(x.getCoffee() - x.getAmecoffee());
        x.setWater(x.getWater() - x.getAmewater());
        x.setTank(x.getTank() + x.getAmecoffee());
        return getAmericano();
    }

    public boolean chekTank(Machine x, int coffeeOneCup) {
        if (x.getTank() + coffeeOneCup > x.getMaxtank()) {
            showMessageTank();
            return false;
        }
        return true;
    }

    public boolean chekWater(Machine x, int waterOneCup) {
        if (waterOneCup > x.getWater()) {
            showMessageWater();
            return false;
        }
        return true;
    }

    public boolean chekCoffee(Machine x, int coffeeOneCup) {
        if (coffeeOneCup > x.getCoffee()) {
            showMessageCoffee();
            return false;
        }
        return true;
    }


    public ExpressoAmericano(String name, int maxwater, int maxcoffee, int maxtank) {
        super(name, maxwater, maxcoffee, maxtank);
    }
}
