import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double i = scanner.nextDouble();


        String s01 = " ***  ";
        String s02 = "*   * ";
        String s03 = "*   * ";
        String s04 = "*   * ";
        String s05 = "*   * ";
        String s06 = "*   * ";
        String s07 = " ***  ";

        String s11 = "  *   ";
        String s12 = " **   ";
        String s13 = "  *   ";
        String s14 = "  *   ";
        String s15 = "  *   ";
        String s16 = "  *   ";
        String s17 = " ***  ";

        String s21 = " ***  ";
        String s22 = "*   * ";
        String s23 = "*  *  ";
        String s24 = "  *   ";
        String s25 = " *    ";
        String s26 = "*     ";
        String s27 = "***** ";

        String s31 = " ***  ";
        String s32 = "*   * ";
        String s33 = "    * ";
        String s34 = " ***  ";
        String s35 = "    * ";
        String s36 = "*   * ";
        String s37 = " ***  ";

        String s41 = "   *  ";
        String s42 = "  **  ";
        String s43 = " * *  ";
        String s44 = "*  *  ";
        String s45 = "***** ";
        String s46 = "   *  ";
        String s47 = "   *  ";

        String s51 = "***** ";
        String s52 = "*     ";
        String s53 = "*     ";
        String s54 = "****  ";
        String s55 = "    * ";
        String s56 = "    * ";
        String s57 = "****  ";

        String s61 = "    * ";
        String s62 = "   *  ";
        String s63 = "  *   ";
        String s64 = " *    ";
        String s65 = "***** ";
        String s66 = "*   * ";
        String s67 = "***** ";

        String s71 = "***** ";
        String s72 = "    * ";
        String s73 = "   *  ";
        String s74 = "  *   ";
        String s75 = " *    ";
        String s76 = "*     ";
        String s77 = "*     ";

        String s81 = " ***  ";
        String s82 = "*   * ";
        String s83 = "*   * ";
        String s84 = " ***  ";
        String s85 = "*   * ";
        String s86 = "*   * ";
        String s87 = " ***  ";

        String s91 = " ***  ";
        String s92 = "*   * ";
        String s93 = "*   * ";
        String s94 = " **** ";
        String s95 = "    * ";
        String s96 = "    * ";
        String s97 = "    * ";

        String end11 = "";
        String end12 = "";
        String end13 = "";
        String end14 = "";
        String end15 = "";
        String end16 = "";
        String end17 = "";
        String end18 = "";

        String end21 = "";
        String end22 = "";
        String end23 = "";
        String end24 = "";
        String end25 = "";
        String end26 = "";
        String end27 = "";
        String end28 = "";

        String end31 = "";
        String end32 = "";
        String end33 = "";
        String end34 = "";
        String end35 = "";
        String end36 = "";
        String end37 = "";
        String end38 = "";

        String end41 = "";
        String end42 = "";
        String end43 = "";
        String end44 = "";
        String end45 = "";
        String end46 = "";
        String end47 = "";
        String end48 = "";

        String end51 = "";
        String end52 = "";
        String end53 = "";
        String end54 = "";
        String end55 = "";
        String end56 = "";
        String end57 = "";
        String end58 = "";

        String end61 = "";
        String end62 = "";
        String end63 = "";
        String end64 = "";
        String end65 = "";
        String end66 = "";
        String end67 = "";
        String end68 = "";

        String end71 = "";
        String end72 = "";
        String end73 = "";
        String end74 = "";
        String end75 = "";
        String end76 = "";
        String end77 = "";
        String end78 = "";


        if (i >= 10000000 && i < 100000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i % 1000000 - i % 100000) / 100000;
            double x7 = (i % 10000000 - i % 1000000) / 1000000;
            double x8 = (i - i % 10000000) / 10000000;


            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }

            if (x4 == 0) {
                end14 = s01;
                end24 = s02;
                end34 = s03;
                end44 = s04;
                end54 = s05;
                end64 = s06;
                end74 = s07;
            } else if (x4 == 1) {
                end14 = s11;
                end24 = s12;
                end34 = s13;
                end44 = s14;
                end54 = s15;
                end64 = s16;
                end74 = s17;
            } else if (x4 == 2) {
                end14 = s21;
                end24 = s22;
                end34 = s23;
                end44 = s24;
                end54 = s25;
                end64 = s26;
                end74 = s27;
            } else if (x4 == 3) {
                end14 = s31;
                end24 = s32;
                end34 = s33;
                end44 = s34;
                end54 = s35;
                end64 = s36;
                end74 = s37;
            } else if (x4 == 4) {
                end14 = s41;
                end24 = s42;
                end34 = s43;
                end44 = s44;
                end54 = s45;
                end64 = s46;
                end74 = s47;
            } else if (x4 == 5) {
                end14 = s51;
                end24 = s52;
                end34 = s53;
                end44 = s54;
                end54 = s55;
                end64 = s56;
                end74 = s57;
            } else if (x4 == 6) {
                end14 = s61;
                end24 = s62;
                end34 = s63;
                end44 = s64;
                end54 = s65;
                end64 = s66;
                end74 = s67;
            } else if (x4 == 7) {
                end14 = s71;
                end24 = s72;
                end34 = s73;
                end44 = s74;
                end54 = s75;
                end64 = s76;
                end74 = s77;
            } else if (x4 == 8) {
                end14 = s81;
                end24 = s82;
                end34 = s83;
                end44 = s84;
                end54 = s85;
                end64 = s86;
                end74 = s87;
            } else if (x4 == 9) {
                end14 = s91;
                end24 = s92;
                end34 = s93;
                end44 = s94;
                end54 = s95;
                end64 = s96;
                end74 = s97;
            }

            if (x5 == 0) {
                end15 = s01;
                end25 = s02;
                end35 = s03;
                end45 = s04;
                end55 = s05;
                end65 = s06;
                end75 = s07;
            } else if (x5 == 1) {
                end15 = s11;
                end25 = s12;
                end35 = s13;
                end45 = s14;
                end55 = s15;
                end65 = s16;
                end75 = s17;
            } else if (x5 == 2) {
                end15 = s21;
                end25 = s22;
                end35 = s23;
                end45 = s24;
                end55 = s25;
                end65 = s26;
                end75 = s27;
            } else if (x5 == 3) {
                end15 = s31;
                end25 = s32;
                end35 = s33;
                end45 = s34;
                end55 = s35;
                end65 = s36;
                end75 = s37;
            } else if (x5 == 4) {
                end15 = s41;
                end25 = s42;
                end35 = s43;
                end45 = s44;
                end55 = s45;
                end65 = s46;
                end75 = s47;
            } else if (x5 == 5) {
                end15 = s51;
                end25 = s52;
                end35 = s53;
                end45 = s54;
                end55 = s55;
                end65 = s56;
                end75 = s57;
            } else if (x5 == 6) {
                end15 = s61;
                end25 = s62;
                end35 = s63;
                end45 = s64;
                end55 = s65;
                end65 = s66;
                end75 = s67;
            } else if (x5 == 7) {
                end15 = s71;
                end25 = s72;
                end35 = s73;
                end45 = s74;
                end55 = s75;
                end65 = s76;
                end75 = s77;
            } else if (x5 == 8) {
                end15 = s81;
                end25 = s82;
                end35 = s83;
                end45 = s84;
                end55 = s85;
                end65 = s86;
                end75 = s87;
            } else if (x5 == 9) {
                end15 = s91;
                end25 = s92;
                end35 = s93;
                end45 = s94;
                end55 = s95;
                end65 = s96;
                end75 = s97;
            }

            if (x6 == 0) {
                end16 = s01;
                end26 = s02;
                end36 = s03;
                end46 = s04;
                end56 = s05;
                end66 = s06;
                end76 = s07;
            } else if (x6 == 1) {
                end16 = s11;
                end26 = s12;
                end36 = s13;
                end46 = s14;
                end56 = s15;
                end66 = s16;
                end76 = s17;
            } else if (x6 == 2) {
                end16 = s21;
                end26 = s22;
                end36 = s23;
                end46 = s24;
                end56 = s25;
                end66 = s26;
                end76 = s27;
            } else if (x6 == 3) {
                end16 = s31;
                end26 = s32;
                end36 = s33;
                end46 = s34;
                end56 = s35;
                end66 = s36;
                end76 = s37;
            } else if (x6 == 4) {
                end16 = s41;
                end26 = s42;
                end36 = s43;
                end46 = s44;
                end56 = s45;
                end66 = s46;
                end76 = s47;
            } else if (x6 == 5) {
                end16 = s51;
                end26 = s52;
                end36 = s53;
                end46 = s54;
                end56 = s55;
                end66 = s56;
                end76 = s57;
            } else if (x6 == 6) {
                end16 = s61;
                end26 = s62;
                end36 = s63;
                end46 = s64;
                end56 = s65;
                end66 = s66;
                end76 = s67;
            } else if (x6 == 7) {
                end16 = s71;
                end26 = s72;
                end36 = s73;
                end46 = s74;
                end56 = s75;
                end66 = s76;
                end76 = s77;
            } else if (x6 == 8) {
                end16 = s81;
                end26 = s82;
                end36 = s83;
                end46 = s84;
                end56 = s85;
                end66 = s86;
                end76 = s87;
            } else if (x6 == 9) {
                end16 = s91;
                end26 = s92;
                end36 = s93;
                end46 = s94;
                end56 = s95;
                end66 = s96;
                end76 = s97;
            }

            if (x7 == 0) {
                end17 = s01;
                end27 = s02;
                end37 = s03;
                end47 = s04;
                end57 = s05;
                end67 = s06;
                end77 = s07;
            } else if (x7 == 1) {
                end17 = s11;
                end27 = s12;
                end37 = s13;
                end47 = s14;
                end57 = s15;
                end67 = s16;
                end77 = s17;
            } else if (x7 == 2) {
                end17 = s21;
                end27 = s22;
                end37 = s23;
                end47 = s24;
                end57 = s25;
                end67 = s26;
                end77 = s27;
            } else if (x7 == 3) {
                end17 = s31;
                end27 = s32;
                end37 = s33;
                end47 = s34;
                end57 = s35;
                end67 = s36;
                end77 = s37;
            } else if (x7 == 4) {
                end17 = s41;
                end27 = s42;
                end37 = s43;
                end47 = s44;
                end57 = s45;
                end67 = s46;
                end77 = s47;
            } else if (x7 == 5) {
                end17 = s51;
                end27 = s52;
                end37 = s53;
                end47 = s54;
                end57 = s55;
                end67 = s56;
                end77 = s57;
            } else if (x7 == 6) {
                end17 = s61;
                end27 = s62;
                end37 = s63;
                end47 = s64;
                end57 = s65;
                end67 = s66;
                end77 = s67;
            } else if (x7 == 7) {
                end17 = s71;
                end27 = s72;
                end37 = s73;
                end47 = s74;
                end57 = s75;
                end67 = s76;
                end77 = s77;
            } else if (x7 == 8) {
                end17 = s81;
                end27 = s82;
                end37 = s83;
                end47 = s84;
                end57 = s85;
                end67 = s86;
                end77 = s87;
            } else if (x7 == 9) {
                end17 = s91;
                end27 = s92;
                end37 = s93;
                end47 = s94;
                end57 = s95;
                end67 = s96;
                end77 = s97;
            }

            if (x8 == 0) {
                end18 = s01;
                end28 = s02;
                end38 = s03;
                end48 = s04;
                end58 = s05;
                end68 = s06;
                end78 = s07;
            } else if (x8 == 1) {
                end18 = s11;
                end28 = s12;
                end38 = s13;
                end48 = s14;
                end58 = s15;
                end68 = s16;
                end78 = s17;
            } else if (x8 == 2) {
                end18 = s21;
                end28 = s22;
                end38 = s23;
                end48 = s24;
                end58 = s25;
                end68 = s26;
                end78 = s27;
            } else if (x8 == 3) {
                end18 = s31;
                end28 = s32;
                end38 = s33;
                end48 = s34;
                end58 = s35;
                end68 = s36;
                end78 = s37;
            } else if (x8 == 4) {
                end18 = s41;
                end28 = s42;
                end38 = s43;
                end48 = s44;
                end58 = s45;
                end68 = s46;
                end78 = s47;
            } else if (x8 == 5) {
                end18 = s51;
                end28 = s52;
                end38 = s53;
                end48 = s54;
                end58 = s55;
                end68 = s56;
                end78 = s57;
            } else if (x8 == 6) {
                end18 = s61;
                end28 = s62;
                end38 = s63;
                end48 = s64;
                end58 = s65;
                end68 = s66;
                end78 = s67;
            } else if (x8 == 7) {
                end18 = s71;
                end28 = s72;
                end38 = s73;
                end48 = s74;
                end58 = s75;
                end68 = s76;
                end78 = s77;
            } else if (x8 == 8) {
                end18 = s81;
                end28 = s82;
                end38 = s83;
                end48 = s84;
                end58 = s85;
                end68 = s86;
                end78 = s87;
            } else if (x8 == 9) {
                end18 = s91;
                end28 = s92;
                end38 = s93;
                end48 = s94;
                end58 = s95;
                end68 = s96;
                end78 = s97;
            }


            String end1 = end18 + end17 + end16 + end15 + end14 + end13 + end12 + end11;
            String end2 = end28 + end27 + end26 + end25 + end24 + end23 + end22 + end21;
            String end3 = end38 + end37 + end36 + end35 + end34 + end33 + end32 + end31;
            String end4 = end48 + end47 + end46 + end45 + end44 + end43 + end42 + end41;
            String end5 = end58 + end57 + end56 + end55 + end54 + end53 + end52 + end51;
            String end6 = end68 + end67 + end66 + end65 + end64 + end63 + end62 + end61;
            String end7 = end78 + end77 + end76 + end75 + end74 + end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);

        }
        else if (i >= 1000000 && i < 10000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i % 1000000 - i % 100000) / 100000;
            double x7 = (i % 10000000 - i % 1000000) / 1000000;


            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }

            if (x4 == 0) {
                end14 = s01;
                end24 = s02;
                end34 = s03;
                end44 = s04;
                end54 = s05;
                end64 = s06;
                end74 = s07;
            } else if (x4 == 1) {
                end14 = s11;
                end24 = s12;
                end34 = s13;
                end44 = s14;
                end54 = s15;
                end64 = s16;
                end74 = s17;
            } else if (x4 == 2) {
                end14 = s21;
                end24 = s22;
                end34 = s23;
                end44 = s24;
                end54 = s25;
                end64 = s26;
                end74 = s27;
            } else if (x4 == 3) {
                end14 = s31;
                end24 = s32;
                end34 = s33;
                end44 = s34;
                end54 = s35;
                end64 = s36;
                end74 = s37;
            } else if (x4 == 4) {
                end14 = s41;
                end24 = s42;
                end34 = s43;
                end44 = s44;
                end54 = s45;
                end64 = s46;
                end74 = s47;
            } else if (x4 == 5) {
                end14 = s51;
                end24 = s52;
                end34 = s53;
                end44 = s54;
                end54 = s55;
                end64 = s56;
                end74 = s57;
            } else if (x4 == 6) {
                end14 = s61;
                end24 = s62;
                end34 = s63;
                end44 = s64;
                end54 = s65;
                end64 = s66;
                end74 = s67;
            } else if (x4 == 7) {
                end14 = s71;
                end24 = s72;
                end34 = s73;
                end44 = s74;
                end54 = s75;
                end64 = s76;
                end74 = s77;
            } else if (x4 == 8) {
                end14 = s81;
                end24 = s82;
                end34 = s83;
                end44 = s84;
                end54 = s85;
                end64 = s86;
                end74 = s87;
            } else if (x4 == 9) {
                end14 = s91;
                end24 = s92;
                end34 = s93;
                end44 = s94;
                end54 = s95;
                end64 = s96;
                end74 = s97;
            }

            if (x5 == 0) {
                end15 = s01;
                end25 = s02;
                end35 = s03;
                end45 = s04;
                end55 = s05;
                end65 = s06;
                end75 = s07;
            } else if (x5 == 1) {
                end15 = s11;
                end25 = s12;
                end35 = s13;
                end45 = s14;
                end55 = s15;
                end65 = s16;
                end75 = s17;
            } else if (x5 == 2) {
                end15 = s21;
                end25 = s22;
                end35 = s23;
                end45 = s24;
                end55 = s25;
                end65 = s26;
                end75 = s27;
            } else if (x5 == 3) {
                end15 = s31;
                end25 = s32;
                end35 = s33;
                end45 = s34;
                end55 = s35;
                end65 = s36;
                end75 = s37;
            } else if (x5 == 4) {
                end15 = s41;
                end25 = s42;
                end35 = s43;
                end45 = s44;
                end55 = s45;
                end65 = s46;
                end75 = s47;
            } else if (x5 == 5) {
                end15 = s51;
                end25 = s52;
                end35 = s53;
                end45 = s54;
                end55 = s55;
                end65 = s56;
                end75 = s57;
            } else if (x5 == 6) {
                end15 = s61;
                end25 = s62;
                end35 = s63;
                end45 = s64;
                end55 = s65;
                end65 = s66;
                end75 = s67;
            } else if (x5 == 7) {
                end15 = s71;
                end25 = s72;
                end35 = s73;
                end45 = s74;
                end55 = s75;
                end65 = s76;
                end75 = s77;
            } else if (x5 == 8) {
                end15 = s81;
                end25 = s82;
                end35 = s83;
                end45 = s84;
                end55 = s85;
                end65 = s86;
                end75 = s87;
            } else if (x5 == 9) {
                end15 = s91;
                end25 = s92;
                end35 = s93;
                end45 = s94;
                end55 = s95;
                end65 = s96;
                end75 = s97;
            }

            if (x6 == 0) {
                end16 = s01;
                end26 = s02;
                end36 = s03;
                end46 = s04;
                end56 = s05;
                end66 = s06;
                end76 = s07;
            } else if (x6 == 1) {
                end16 = s11;
                end26 = s12;
                end36 = s13;
                end46 = s14;
                end56 = s15;
                end66 = s16;
                end76 = s17;
            } else if (x6 == 2) {
                end16 = s21;
                end26 = s22;
                end36 = s23;
                end46 = s24;
                end56 = s25;
                end66 = s26;
                end76 = s27;
            } else if (x6 == 3) {
                end16 = s31;
                end26 = s32;
                end36 = s33;
                end46 = s34;
                end56 = s35;
                end66 = s36;
                end76 = s37;
            } else if (x6 == 4) {
                end16 = s41;
                end26 = s42;
                end36 = s43;
                end46 = s44;
                end56 = s45;
                end66 = s46;
                end76 = s47;
            } else if (x6 == 5) {
                end16 = s51;
                end26 = s52;
                end36 = s53;
                end46 = s54;
                end56 = s55;
                end66 = s56;
                end76 = s57;
            } else if (x6 == 6) {
                end16 = s61;
                end26 = s62;
                end36 = s63;
                end46 = s64;
                end56 = s65;
                end66 = s66;
                end76 = s67;
            } else if (x6 == 7) {
                end16 = s71;
                end26 = s72;
                end36 = s73;
                end46 = s74;
                end56 = s75;
                end66 = s76;
                end76 = s77;
            } else if (x6 == 8) {
                end16 = s81;
                end26 = s82;
                end36 = s83;
                end46 = s84;
                end56 = s85;
                end66 = s86;
                end76 = s87;
            } else if (x6 == 9) {
                end16 = s91;
                end26 = s92;
                end36 = s93;
                end46 = s94;
                end56 = s95;
                end66 = s96;
                end76 = s97;
            }

            if (x7 == 0) {
                end17 = s01;
                end27 = s02;
                end37 = s03;
                end47 = s04;
                end57 = s05;
                end67 = s06;
                end77 = s07;
            } else if (x7 == 1) {
                end17 = s11;
                end27 = s12;
                end37 = s13;
                end47 = s14;
                end57 = s15;
                end67 = s16;
                end77 = s17;
            } else if (x7 == 2) {
                end17 = s21;
                end27 = s22;
                end37 = s23;
                end47 = s24;
                end57 = s25;
                end67 = s26;
                end77 = s27;
            } else if (x7 == 3) {
                end17 = s31;
                end27 = s32;
                end37 = s33;
                end47 = s34;
                end57 = s35;
                end67 = s36;
                end77 = s37;
            } else if (x7 == 4) {
                end17 = s41;
                end27 = s42;
                end37 = s43;
                end47 = s44;
                end57 = s45;
                end67 = s46;
                end77 = s47;
            } else if (x7 == 5) {
                end17 = s51;
                end27 = s52;
                end37 = s53;
                end47 = s54;
                end57 = s55;
                end67 = s56;
                end77 = s57;
            } else if (x7 == 6) {
                end17 = s61;
                end27 = s62;
                end37 = s63;
                end47 = s64;
                end57 = s65;
                end67 = s66;
                end77 = s67;
            } else if (x7 == 7) {
                end17 = s71;
                end27 = s72;
                end37 = s73;
                end47 = s74;
                end57 = s75;
                end67 = s76;
                end77 = s77;
            } else if (x7 == 8) {
                end17 = s81;
                end27 = s82;
                end37 = s83;
                end47 = s84;
                end57 = s85;
                end67 = s86;
                end77 = s87;
            } else if (x7 == 9) {
                end17 = s91;
                end27 = s92;
                end37 = s93;
                end47 = s94;
                end57 = s95;
                end67 = s96;
                end77 = s97;
            }


            String end1 = end17 + end16 + end15 + end14 + end13 + end12 + end11;
            String end2 = end27 + end26 + end25 + end24 + end23 + end22 + end21;
            String end3 = end37 + end36 + end35 + end34 + end33 + end32 + end31;
            String end4 = end47 + end46 + end45 + end44 + end43 + end42 + end41;
            String end5 = end57 + end56 + end55 + end54 + end53 + end52 + end51;
            String end6 = end67 + end66 + end65 + end64 + end63 + end62 + end61;
            String end7 = end77 + end76 + end75 + end74 + end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);

        }
        else if (i >= 100000 && i < 1000000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;
            double x6 = (i % 1000000 - i % 100000) / 100000;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }

            if (x4 == 0) {
                end14 = s01;
                end24 = s02;
                end34 = s03;
                end44 = s04;
                end54 = s05;
                end64 = s06;
                end74 = s07;
            } else if (x4 == 1) {
                end14 = s11;
                end24 = s12;
                end34 = s13;
                end44 = s14;
                end54 = s15;
                end64 = s16;
                end74 = s17;
            } else if (x4 == 2) {
                end14 = s21;
                end24 = s22;
                end34 = s23;
                end44 = s24;
                end54 = s25;
                end64 = s26;
                end74 = s27;
            } else if (x4 == 3) {
                end14 = s31;
                end24 = s32;
                end34 = s33;
                end44 = s34;
                end54 = s35;
                end64 = s36;
                end74 = s37;
            } else if (x4 == 4) {
                end14 = s41;
                end24 = s42;
                end34 = s43;
                end44 = s44;
                end54 = s45;
                end64 = s46;
                end74 = s47;
            } else if (x4 == 5) {
                end14 = s51;
                end24 = s52;
                end34 = s53;
                end44 = s54;
                end54 = s55;
                end64 = s56;
                end74 = s57;
            } else if (x4 == 6) {
                end14 = s61;
                end24 = s62;
                end34 = s63;
                end44 = s64;
                end54 = s65;
                end64 = s66;
                end74 = s67;
            } else if (x4 == 7) {
                end14 = s71;
                end24 = s72;
                end34 = s73;
                end44 = s74;
                end54 = s75;
                end64 = s76;
                end74 = s77;
            } else if (x4 == 8) {
                end14 = s81;
                end24 = s82;
                end34 = s83;
                end44 = s84;
                end54 = s85;
                end64 = s86;
                end74 = s87;
            } else if (x4 == 9) {
                end14 = s91;
                end24 = s92;
                end34 = s93;
                end44 = s94;
                end54 = s95;
                end64 = s96;
                end74 = s97;
            }

            if (x5 == 0) {
                end15 = s01;
                end25 = s02;
                end35 = s03;
                end45 = s04;
                end55 = s05;
                end65 = s06;
                end75 = s07;
            } else if (x5 == 1) {
                end15 = s11;
                end25 = s12;
                end35 = s13;
                end45 = s14;
                end55 = s15;
                end65 = s16;
                end75 = s17;
            } else if (x5 == 2) {
                end15 = s21;
                end25 = s22;
                end35 = s23;
                end45 = s24;
                end55 = s25;
                end65 = s26;
                end75 = s27;
            } else if (x5 == 3) {
                end15 = s31;
                end25 = s32;
                end35 = s33;
                end45 = s34;
                end55 = s35;
                end65 = s36;
                end75 = s37;
            } else if (x5 == 4) {
                end15 = s41;
                end25 = s42;
                end35 = s43;
                end45 = s44;
                end55 = s45;
                end65 = s46;
                end75 = s47;
            } else if (x5 == 5) {
                end15 = s51;
                end25 = s52;
                end35 = s53;
                end45 = s54;
                end55 = s55;
                end65 = s56;
                end75 = s57;
            } else if (x5 == 6) {
                end15 = s61;
                end25 = s62;
                end35 = s63;
                end45 = s64;
                end55 = s65;
                end65 = s66;
                end75 = s67;
            } else if (x5 == 7) {
                end15 = s71;
                end25 = s72;
                end35 = s73;
                end45 = s74;
                end55 = s75;
                end65 = s76;
                end75 = s77;
            } else if (x5 == 8) {
                end15 = s81;
                end25 = s82;
                end35 = s83;
                end45 = s84;
                end55 = s85;
                end65 = s86;
                end75 = s87;
            } else if (x5 == 9) {
                end15 = s91;
                end25 = s92;
                end35 = s93;
                end45 = s94;
                end55 = s95;
                end65 = s96;
                end75 = s97;
            }

            if (x6 == 0) {
                end16 = s01;
                end26 = s02;
                end36 = s03;
                end46 = s04;
                end56 = s05;
                end66 = s06;
                end76 = s07;
            } else if (x6 == 1) {
                end16 = s11;
                end26 = s12;
                end36 = s13;
                end46 = s14;
                end56 = s15;
                end66 = s16;
                end76 = s17;
            } else if (x6 == 2) {
                end16 = s21;
                end26 = s22;
                end36 = s23;
                end46 = s24;
                end56 = s25;
                end66 = s26;
                end76 = s27;
            } else if (x6 == 3) {
                end16 = s31;
                end26 = s32;
                end36 = s33;
                end46 = s34;
                end56 = s35;
                end66 = s36;
                end76 = s37;
            } else if (x6 == 4) {
                end16 = s41;
                end26 = s42;
                end36 = s43;
                end46 = s44;
                end56 = s45;
                end66 = s46;
                end76 = s47;
            } else if (x6 == 5) {
                end16 = s51;
                end26 = s52;
                end36 = s53;
                end46 = s54;
                end56 = s55;
                end66 = s56;
                end76 = s57;
            } else if (x6 == 6) {
                end16 = s61;
                end26 = s62;
                end36 = s63;
                end46 = s64;
                end56 = s65;
                end66 = s66;
                end76 = s67;
            } else if (x6 == 7) {
                end16 = s71;
                end26 = s72;
                end36 = s73;
                end46 = s74;
                end56 = s75;
                end66 = s76;
                end76 = s77;
            } else if (x6 == 8) {
                end16 = s81;
                end26 = s82;
                end36 = s83;
                end46 = s84;
                end56 = s85;
                end66 = s86;
                end76 = s87;
            } else if (x6 == 9) {
                end16 = s91;
                end26 = s92;
                end36 = s93;
                end46 = s94;
                end56 = s95;
                end66 = s96;
                end76 = s97;
            }


            String end1 = end16 + end15 + end14 + end13 + end12 + end11;
            String end2 = end26 + end25 + end24 + end23 + end22 + end21;
            String end3 = end36 + end35 + end34 + end33 + end32 + end31;
            String end4 = end46 + end45 + end44 + end43 + end42 + end41;
            String end5 = end56 + end55 + end54 + end53 + end52 + end51;
            String end6 = end66 + end65 + end64 + end63 + end62 + end61;
            String end7 = end76 + end75 + end74 + end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);

        }
        else if (i >= 10000 && i < 100000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;
            double x5 = (i % 100000 - i % 10000) / 10000;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }

            if (x4 == 0) {
                end14 = s01;
                end24 = s02;
                end34 = s03;
                end44 = s04;
                end54 = s05;
                end64 = s06;
                end74 = s07;
            } else if (x4 == 1) {
                end14 = s11;
                end24 = s12;
                end34 = s13;
                end44 = s14;
                end54 = s15;
                end64 = s16;
                end74 = s17;
            } else if (x4 == 2) {
                end14 = s21;
                end24 = s22;
                end34 = s23;
                end44 = s24;
                end54 = s25;
                end64 = s26;
                end74 = s27;
            } else if (x4 == 3) {
                end14 = s31;
                end24 = s32;
                end34 = s33;
                end44 = s34;
                end54 = s35;
                end64 = s36;
                end74 = s37;
            } else if (x4 == 4) {
                end14 = s41;
                end24 = s42;
                end34 = s43;
                end44 = s44;
                end54 = s45;
                end64 = s46;
                end74 = s47;
            } else if (x4 == 5) {
                end14 = s51;
                end24 = s52;
                end34 = s53;
                end44 = s54;
                end54 = s55;
                end64 = s56;
                end74 = s57;
            } else if (x4 == 6) {
                end14 = s61;
                end24 = s62;
                end34 = s63;
                end44 = s64;
                end54 = s65;
                end64 = s66;
                end74 = s67;
            } else if (x4 == 7) {
                end14 = s71;
                end24 = s72;
                end34 = s73;
                end44 = s74;
                end54 = s75;
                end64 = s76;
                end74 = s77;
            } else if (x4 == 8) {
                end14 = s81;
                end24 = s82;
                end34 = s83;
                end44 = s84;
                end54 = s85;
                end64 = s86;
                end74 = s87;
            } else if (x4 == 9) {
                end14 = s91;
                end24 = s92;
                end34 = s93;
                end44 = s94;
                end54 = s95;
                end64 = s96;
                end74 = s97;
            }

            if (x5 == 0) {
                end15 = s01;
                end25 = s02;
                end35 = s03;
                end45 = s04;
                end55 = s05;
                end65 = s06;
                end75 = s07;
            } else if (x5 == 1) {
                end15 = s11;
                end25 = s12;
                end35 = s13;
                end45 = s14;
                end55 = s15;
                end65 = s16;
                end75 = s17;
            } else if (x5 == 2) {
                end15 = s21;
                end25 = s22;
                end35 = s23;
                end45 = s24;
                end55 = s25;
                end65 = s26;
                end75 = s27;
            } else if (x5 == 3) {
                end15 = s31;
                end25 = s32;
                end35 = s33;
                end45 = s34;
                end55 = s35;
                end65 = s36;
                end75 = s37;
            } else if (x5 == 4) {
                end15 = s41;
                end25 = s42;
                end35 = s43;
                end45 = s44;
                end55 = s45;
                end65 = s46;
                end75 = s47;
            } else if (x5 == 5) {
                end15 = s51;
                end25 = s52;
                end35 = s53;
                end45 = s54;
                end55 = s55;
                end65 = s56;
                end75 = s57;
            } else if (x5 == 6) {
                end15 = s61;
                end25 = s62;
                end35 = s63;
                end45 = s64;
                end55 = s65;
                end65 = s66;
                end75 = s67;
            } else if (x5 == 7) {
                end15 = s71;
                end25 = s72;
                end35 = s73;
                end45 = s74;
                end55 = s75;
                end65 = s76;
                end75 = s77;
            } else if (x5 == 8) {
                end15 = s81;
                end25 = s82;
                end35 = s83;
                end45 = s84;
                end55 = s85;
                end65 = s86;
                end75 = s87;
            } else if (x5 == 9) {
                end15 = s91;
                end25 = s92;
                end35 = s93;
                end45 = s94;
                end55 = s95;
                end65 = s96;
                end75 = s97;
            }

            String end1 = end15 + end14 + end13 + end12 + end11;
            String end2 = end25 + end24 + end23 + end22 + end21;
            String end3 = end35 + end34 + end33 + end32 + end31;
            String end4 = end45 + end44 + end43 + end42 + end41;
            String end5 = end55 + end54 + end53 + end52 + end51;
            String end6 = end65 + end64 + end63 + end62 + end61;
            String end7 = end75 + end74 + end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);

        }
        else if (i >= 1000 && i < 10000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;
            double x4 = (i % 10000 - i % 1000) / 1000;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }

            if (x4 == 0) {
                end14 = s01;
                end24 = s02;
                end34 = s03;
                end44 = s04;
                end54 = s05;
                end64 = s06;
                end74 = s07;
            } else if (x4 == 1) {
                end14 = s11;
                end24 = s12;
                end34 = s13;
                end44 = s14;
                end54 = s15;
                end64 = s16;
                end74 = s17;
            } else if (x4 == 2) {
                end14 = s21;
                end24 = s22;
                end34 = s23;
                end44 = s24;
                end54 = s25;
                end64 = s26;
                end74 = s27;
            } else if (x4 == 3) {
                end14 = s31;
                end24 = s32;
                end34 = s33;
                end44 = s34;
                end54 = s35;
                end64 = s36;
                end74 = s37;
            } else if (x4 == 4) {
                end14 = s41;
                end24 = s42;
                end34 = s43;
                end44 = s44;
                end54 = s45;
                end64 = s46;
                end74 = s47;
            } else if (x4 == 5) {
                end14 = s51;
                end24 = s52;
                end34 = s53;
                end44 = s54;
                end54 = s55;
                end64 = s56;
                end74 = s57;
            } else if (x4 == 6) {
                end14 = s61;
                end24 = s62;
                end34 = s63;
                end44 = s64;
                end54 = s65;
                end64 = s66;
                end74 = s67;
            } else if (x4 == 7) {
                end14 = s71;
                end24 = s72;
                end34 = s73;
                end44 = s74;
                end54 = s75;
                end64 = s76;
                end74 = s77;
            } else if (x4 == 8) {
                end14 = s81;
                end24 = s82;
                end34 = s83;
                end44 = s84;
                end54 = s85;
                end64 = s86;
                end74 = s87;
            } else if (x4 == 9) {
                end14 = s91;
                end24 = s92;
                end34 = s93;
                end44 = s94;
                end54 = s95;
                end64 = s96;
                end74 = s97;
            }


            String end1 = end14 + end13 + end12 + end11;
            String end2 = end24 + end23 + end22 + end21;
            String end3 = end34 + end33 + end32 + end31;
            String end4 = end44 + end43 + end42 + end41;
            String end5 = end54 + end53 + end52 + end51;
            String end6 = end64 + end63 + end62 + end61;
            String end7 = end74 + end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);
        }
        else if (i >= 100 && i < 1000) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;
            double x3 = (i % 1000 - i % 100) / 100;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            if (x3 == 0) {
                end13 = s01;
                end23 = s02;
                end33 = s03;
                end43 = s04;
                end53 = s05;
                end63 = s06;
                end73 = s07;
            } else if (x3 == 1) {
                end13 = s11;
                end23 = s12;
                end33 = s13;
                end43 = s14;
                end53 = s15;
                end63 = s16;
                end73 = s17;
            } else if (x3 == 2) {
                end13 = s21;
                end23 = s22;
                end33 = s23;
                end43 = s24;
                end53 = s25;
                end63 = s26;
                end73 = s27;
            } else if (x3 == 3) {
                end13 = s31;
                end23 = s32;
                end33 = s33;
                end43 = s34;
                end53 = s35;
                end63 = s36;
                end73 = s37;
            } else if (x3 == 4) {
                end13 = s41;
                end23 = s42;
                end33 = s43;
                end43 = s44;
                end53 = s45;
                end63 = s46;
                end73 = s47;
            } else if (x3 == 5) {
                end13 = s51;
                end23 = s52;
                end33 = s53;
                end43 = s54;
                end53 = s55;
                end63 = s56;
                end73 = s57;
            } else if (x3 == 6) {
                end13 = s61;
                end23 = s62;
                end33 = s63;
                end43 = s64;
                end53 = s65;
                end63 = s66;
                end73 = s67;
            } else if (x3 == 7) {
                end13 = s71;
                end23 = s72;
                end33 = s73;
                end43 = s74;
                end53 = s75;
                end63 = s76;
                end73 = s77;
            } else if (x3 == 8) {
                end13 = s81;
                end23 = s82;
                end33 = s83;
                end43 = s84;
                end53 = s85;
                end63 = s86;
                end73 = s87;
            } else if (x3 == 9) {
                end13 = s91;
                end23 = s92;
                end33 = s93;
                end43 = s94;
                end53 = s95;
                end63 = s96;
                end73 = s97;
            }


            String end1 = end13 + end12 + end11;
            String end2 = end23 + end22 + end21;
            String end3 = end33 + end32 + end31;
            String end4 = end43 + end42 + end41;
            String end5 = end53 + end52 + end51;
            String end6 = end63 + end62 + end61;
            String end7 = end73 + end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);
        }
        else if (i >= 10 && i < 100) {
            double x1 = i % 10;
            double x2 = (i % 100 - x1) / 10;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }


            if (x2 == 0) {
                end12 = s01;
                end22 = s02;
                end32 = s03;
                end42 = s04;
                end52 = s05;
                end62 = s06;
                end72 = s07;
            } else if (x2 == 1) {
                end12 = s11;
                end22 = s12;
                end32 = s13;
                end42 = s14;
                end52 = s15;
                end62 = s16;
                end72 = s17;
            } else if (x2 == 2) {
                end12 = s21;
                end22 = s22;
                end32 = s23;
                end42 = s24;
                end52 = s25;
                end62 = s26;
                end72 = s27;
            } else if (x2 == 3) {
                end12 = s31;
                end22 = s32;
                end32 = s33;
                end42 = s34;
                end52 = s35;
                end62 = s36;
                end72 = s37;
            } else if (x2 == 4) {
                end12 = s41;
                end22 = s42;
                end32 = s43;
                end42 = s44;
                end52 = s45;
                end62 = s46;
                end72 = s47;
            } else if (x2 == 5) {
                end12 = s51;
                end22 = s52;
                end32 = s53;
                end42 = s54;
                end52 = s55;
                end62 = s56;
                end72 = s57;
            } else if (x2 == 6) {
                end12 = s61;
                end22 = s62;
                end32 = s63;
                end42 = s64;
                end52 = s65;
                end62 = s66;
                end72 = s67;
            } else if (x2 == 7) {
                end12 = s71;
                end22 = s72;
                end32 = s73;
                end42 = s74;
                end52 = s75;
                end62 = s76;
                end72 = s77;
            } else if (x2 == 8) {
                end12 = s81;
                end22 = s82;
                end32 = s83;
                end42 = s84;
                end52 = s85;
                end62 = s86;
                end72 = s87;
            } else if (x2 == 9) {
                end12 = s91;
                end22 = s92;
                end32 = s93;
                end42 = s94;
                end52 = s95;
                end62 = s96;
                end72 = s97;
            }


            String end1 = end12 + end11;
            String end2 = end22 + end21;
            String end3 = end32 + end31;
            String end4 = end42 + end41;
            String end5 = end52 + end51;
            String end6 = end62 + end61;
            String end7 = end72 + end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);
        }
        else if (i >= 0 && i < 10) {
            double x1 = i;

            if (x1 == 0) {
                end11 = s01;
                end21 = s02;
                end31 = s03;
                end41 = s04;
                end51 = s05;
                end61 = s06;
                end71 = s07;
            } else if (x1 == 1) {
                end11 = s11;
                end21 = s12;
                end31 = s13;
                end41 = s14;
                end51 = s15;
                end61 = s16;
                end71 = s17;
            } else if (x1 == 2) {
                end11 = s21;
                end21 = s22;
                end31 = s23;
                end41 = s24;
                end51 = s25;
                end61 = s26;
                end71 = s27;
            } else if (x1 == 3) {
                end11 = s31;
                end21 = s32;
                end31 = s33;
                end41 = s34;
                end51 = s35;
                end61 = s36;
                end71 = s37;
            } else if (x1 == 4) {
                end11 = s41;
                end21 = s42;
                end31 = s43;
                end41 = s44;
                end51 = s45;
                end61 = s46;
                end71 = s47;
            } else if (x1 == 5) {
                end11 = s51;
                end21 = s52;
                end31 = s53;
                end41 = s54;
                end51 = s55;
                end61 = s56;
                end71 = s57;
            } else if (x1 == 6) {
                end11 = s61;
                end21 = s62;
                end31 = s63;
                end41 = s64;
                end51 = s65;
                end61 = s66;
                end71 = s67;
            } else if (x1 == 7) {
                end11 = s71;
                end21 = s72;
                end31 = s73;
                end41 = s74;
                end51 = s75;
                end61 = s76;
                end71 = s77;
            } else if (x1 == 8) {
                end11 = s81;
                end21 = s82;
                end31 = s83;
                end41 = s84;
                end51 = s85;
                end61 = s86;
                end71 = s87;
            } else if (x1 == 9) {
                end11 = s91;
                end21 = s92;
                end31 = s93;
                end41 = s94;
                end51 = s95;
                end61 = s96;
                end71 = s97;
            }

            String end1 = end11;
            String end2 = end21;
            String end3 = end31;
            String end4 = end41;
            String end5 = end51;
            String end6 = end61;
            String end7 = end71;
            System.out.println(end1);
            System.out.println(end2);
            System.out.println(end3);
            System.out.println(end4);
            System.out.println(end5);
            System.out.println(end6);
            System.out.println(end7);
        }
    }
}
