import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String res = "";
        if (str.contains("0") || str.contains("1") || str.contains("2") || str.contains("3") || str.contains("4") || str.contains("5") || str.contains("6") || str.contains("7") || str.contains("8") || str.contains("9")) {
            str = str.replace("0", "_");
            str = str.replace("1", "_");
            str = str.replace("2", "_");
            str = str.replace("3", "_");
            str = str.replace("4", "_");
            str = str.replace("5", "_");
            str = str.replace("6", "_");
            str = str.replace("7", "_");
            str = str.replace("8", "_");
            str = str.replace("9", "_");
        }
        for (int i = 0; i < str.length(); i++) {
            Character ch = str.charAt(i);
            if (Character.isUpperCase(ch)) {
                res += Character.toLowerCase(ch);
            } else
                res += Character.toUpperCase(ch);
        }
        System.out.println(res);
    }

}

