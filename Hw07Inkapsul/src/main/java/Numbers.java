public class Numbers {
    private double a;
    private double b;
    private char i = 'i';

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public char getI() {
        return i;
    }

    public void setI(char i) {
        this.i = i;
    }

    public Numbers(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Numbers(int a, int b) {
        this.a = (double) a;
        this.b = (double) b;
    }

    public Numbers(int a, double b) {
        this.a = (double) a;
        this.b = b;
    }

    public Numbers(double a, int b) {
        this.a = a;
        this.b = (double) b;
    }

    public static void toSrting(Numbers x) {
        System.out.println(x.a + "+" + x.b + "*" + x.i);
    }

    public static void sumOfNum(Numbers x, Numbers y) {
        double a = x.a + y.a;
        double b = x.b + y.b;
        System.out.println(a + "+" + b + "*" + x.i);
    }

    public static void subtractionOfNum(Numbers x, Numbers y) {
        double a = x.a - y.a;
        double b = x.b - y.b;
        System.out.println(a + "+" + b + "*" + x.i);
    }

    public static void multOfNum(Numbers x, Numbers y) {
        double a = x.a * y.a;
        double b = x.b * y.b;
        System.out.println(a + "+" + b + "*" + x.i);
    }

    public static void comparisonOfNum(Numbers x, Numbers y) {
        if (x.a+x.b > y.a+y.b){
            System.out.println("The first number is greater.");
        }
        else {
            System.out.println("The second number is greater.");
        }
    }


}
