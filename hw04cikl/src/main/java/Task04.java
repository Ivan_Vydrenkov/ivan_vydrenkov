public class Task04 {
    public static void main(String[] args) {
        int sum = 0;
        for (double i = 0; i <= 1000000; i++) {
            for (int divider = 1; divider < i; divider++) {
                if (i % divider == 0) {
                    sum = sum + divider;
                }
            }
            if (sum == i) {
                System.out.println(sum);
                sum = 0;
            } else sum = 0;
        }
    }
}
