public class hw02task11
{
    public static void main(String[] args)
    {
        int t = 10800;
        int d = t / 86400;
        int a = t / 3600 - d * 24;
        int m = t / 60 - (a * 60) - (d * 1440);
        int s = t % 60;
        System.out.println("До Нового Года осталось " + d + " дней, " + a + " часов, " + m + " минут, " + s + " секунд.");
    }
}
