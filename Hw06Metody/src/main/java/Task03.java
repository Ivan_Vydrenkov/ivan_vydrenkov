import java.util.*;

//В массиве хранится n явно заданных текстовых строк. Создать метод:
//        +выводящий содержимое массива в строку через пробел;
//        +сортирующий массив в обратном порядке (без учета регистра) от z до a;
//        +сортирующий массив по количеству слов в строке (слова разделены пробелами).
//        +Дополнительно: 1 балл за генерацию случайных уникальных строк реализованных в виде метода.

public class Task03 {
    public static String getNewString() {
        String SALTCHARS = "abcdfghi ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 30) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

public static String [] sortArray (String [] array){
    for (int i = 0; i < array.length; i++) {
        String first = array[i];
        StringTokenizer str = new StringTokenizer(first);
        double firstsum = str.countTokens();
        for (int j = i - 1; j >= 0; j--) {
            String second = array[j];
            StringTokenizer str2 = new StringTokenizer(second);
            double secondsum = str2.countTokens();
            if (firstsum > secondsum) {
                array[j + 1] = second;
                array[j] = first;
            } else {
                break;
            }
        }
    }
    return array;
}

public static void toString (String [] array){
    String intArrayString = Arrays.toString(array);
    System.out.println(intArrayString);
}

    public static String [] sortReverse (String [] array){
        Arrays.sort(array, Collections.reverseOrder());
        return array;
    }

    public static void main(String[] args) {
        int size = 8;
        String[] array = new String[size];
        for (int i = 0; i < size; i++) {
            array[i] = getNewString();
        }
       toString(array);
        array = sortReverse(array);
        System.out.println(Arrays.toString(array));
        array = sortArray(array);
        System.out.println(Arrays.toString(array));
    }

}
