import java.util.Arrays;

//Напишите метод бинарного поиска в одномерном массиве. В случае, если массив не отсортирован, метод должен
//бросать проверяемое исключение.

public class Task04 {
    public static void main(String[] args) {
       int [] arr = createRandomArr();
        System.out.println("_____");

        Arrays.sort(arr);
        try {
            System.out.println(binarySearch(arr,5));
        } catch (checkedException e) {
            System.err.println("Not sort array");
        }
        System.out.println("_____");
        System.out.println("_____");

        int [] arr2 = createRandomArr();
        try {
            System.out.println(binarySearch(arr2, 5));
        } catch (checkedException e) {
            System.err.println("Not sort array");
        }
    }
    public static boolean checkArraySort(int [] arr){
        for (int i=0;  i<arr.length-1; i++){
            if (arr[i]>arr[i+1])
                return false;
        }
        return true;
    }

    public static int[] createRandomArr() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 20));
            System.out.println(array[i]);
        }
        return array;
    }


    public static int binarySearch(int arr[], int elementToSearch) throws checkedException {
        if (checkArraySort(arr)!=true){
            throw new checkedException("Not sort array");
        }
        int firstIndex = 0;
        int lastIndex = arr.length - 1;

        while(firstIndex <= lastIndex) {
            int middleIndex = (firstIndex + lastIndex) / 2;

            if (arr[middleIndex] == elementToSearch) {
                return middleIndex;
            }
            else if (arr[middleIndex] < elementToSearch)
                firstIndex = middleIndex + 1;

            else if (arr[middleIndex] > elementToSearch)
                lastIndex = middleIndex - 1;

        }
        return -1;
    }
}
