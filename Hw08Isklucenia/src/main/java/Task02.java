import java.util.Scanner;
//Создать метод, принимающий на вход число. В случае, если число отрицательное, в методе должно быть брошено
//проверяемое исключение. Если число больше 100, должно быть брошено непроверяемое исключение. Создать свои
// исключения для данного примера.
public class Task02 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        try {
            checkNumber(scn.nextInt());
        } catch (checkedException e) {
            System.err.println("Number greater than 100");
            ;
        }
    }

    public static void checkNumber(int i) throws checkedException {
        if (i < 0) {
            throw new checkedException("Negative number");
        }
        if (i > 100) {
            throw new uncheckedException("Number greater than 100");
        }
    }
}
