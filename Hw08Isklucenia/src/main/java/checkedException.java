public class checkedException extends Exception {
    public checkedException() {
    }

    public checkedException(String message) {
        super(message);
    }

    public checkedException(String message, Throwable cause) {
        super(message, cause);
    }

    public checkedException(Throwable cause) {
        super(cause);
    }

    public checkedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
