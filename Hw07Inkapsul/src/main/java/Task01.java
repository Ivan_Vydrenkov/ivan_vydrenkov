import java.util.Scanner;

// TASK 01      Создать программу, которая позволяет ввести с клавиатуры n диапазонов целочисленных значений (например,
//              3-10). Каждый диапазон имеет начальную и конечную точку. После ввода программа должна вывести длину каждого диа-
//              пазона. Программа должна выводить сообщение об ошибке, если начало диапазона больше, чем конец.

//TASK 02       Для задания 1 добавить возможность проверки, пересекаются ли различные диапазоны значений.

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        Intervals intervals = new Intervals(str);
        Intervals.toStringIntervals(intervals);
        Intervals.coincidences(intervals);
    }
}
