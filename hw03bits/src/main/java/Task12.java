import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();
        System.out.println(Integer.toBinaryString(num));
        int sum = 0;
        while (num != 0) {
            if ((num & 1) == 0) {
                sum += 1;
            }
            num >>>= 1;
        }
        if (sum > 0) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
