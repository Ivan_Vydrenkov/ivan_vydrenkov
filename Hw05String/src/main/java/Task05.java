import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = str1.replace(" ", ",");
        str2 = str2.replace(",,", ",");
        String[] arr = str2.split(",");
        for (int i = 0; i < arr.length; i++) {
            double word = arr[i].length();
            if (word % 2 == 0) {
                sum = sum + 1;
            }
        }
        System.out.println(sum);
    }
}
