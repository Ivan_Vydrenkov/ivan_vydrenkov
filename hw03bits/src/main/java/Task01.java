import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num1 = scan.nextInt();
        System.out.println(Integer.toBinaryString(num1));

        int num2 = num1 & ~(1);
        System.out.println("New number: ");
        System.out.println(Integer.toBinaryString(num2));
    }
}
