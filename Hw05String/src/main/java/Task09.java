import java.util.Arrays;
import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        str1 = str1.replaceAll("[^a-zA-Z ]", "");
        str2 = str2.replaceAll("[^a-zA-Z ]", "");
        char[] word1 = str1.toCharArray();
        char[] word2 = str2.toCharArray();
        Arrays.sort(word1);
        Arrays.sort(word2);
        if (Arrays.equals(word1, word2)) {
            System.out.println("Yes");
        } else System.out.println("No");
    }
}
