public class Task03 {
    public static void main(String[] args) {
//        task03 Машина варит американо и латте из зернового кофе:
        AmericanoLatte model03 = new AmericanoLatte("Model 03", 2000, 500, 500, 800, 500);
        model03.setWater(2000);
        model03.setBeanstank(500);
        model03.setMilk(800);
        ButtonOn.onMachine(model03);
        model03.cookAmerikani(model03);
        model03.cookLatte(model03);
    }
}
