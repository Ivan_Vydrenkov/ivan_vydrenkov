public class Task06 {

//    В библиотеке решили автоматизировать учет книг, выданных студентам.
//    Студент, для того чтобы получить книгу, должен получить читательский билет, если не получал ранее.
//    Разработать модель программы, которая бы позволила находить должников.

    public static void main(String[] args) {
        Ticket one = new Ticket(1, "Ivan", "Vydrenkov", 02063);
        Journal onejournal = new Journal("Stalker", one, 1, 9, 13);
        onejournal.setMontheback(12);
        onejournal.setDayback(12);
        Journal.checkTicket(onejournal);
    }
}
