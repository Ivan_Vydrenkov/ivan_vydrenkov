import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number:");
        int num = scan.nextInt();

        System.out.println(String.format("%32s", Integer.toBinaryString(num)).replace(' ', '0'));

        System.out.println("Enter bit number:");
        int bit = scan.nextInt();

        int num2 = num ^ (1 << bit); //  Младший бит имеет номер 0
        System.out.println("New number:" + "\n" + String.format("%32s", Integer.toBinaryString(num2)).replace(' ', '0'));
    }
}
