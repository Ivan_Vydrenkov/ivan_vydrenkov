import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a power of two:");
        int power = scan.nextInt();
        int num = 2 << (power - 1);
        System.out.println(String.format("%32s", Integer.toBinaryString(num)).replace(' ', '0'));

    }
}
