import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first number:");
        int first = scan.nextInt();
        System.out.println("Enter the second number:");
        int second = scan.nextInt();

        int znak = first ^ second;

        if (znak >= 0) {
            System.out.println("Same.");
        } else {
            System.out.println("Different.");
        }

    }
}
