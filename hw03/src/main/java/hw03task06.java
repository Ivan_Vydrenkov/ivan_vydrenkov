import java.util.Scanner;

public class hw03task06 {
    public static void main(String[] args) {
        System.out.print("Enter the year: ");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        float r400 = year % 400;
        int rem400 = (int) r400;
        float r = year % 4;
        int rem = (int) r;
        if (rem == 0 && rem400 != 0)
            System.out.print("366");
        else System.out.println("365");
    }
}
