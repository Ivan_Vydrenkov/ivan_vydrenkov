import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        int days = 0;
        Scanner scanner = new Scanner(System.in);
        for (double i = scanner.nextDouble(); i > 10; i = i - i * 0.1) {
            days++;
        }
        System.out.println(days+1);
    }
}
