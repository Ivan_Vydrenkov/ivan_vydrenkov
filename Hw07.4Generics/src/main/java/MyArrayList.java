import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class MyArrayList<T> {

    private int size;
    private int capacity;
    private T[] data;

    public MyArrayList() {
        capacity = 10;
        data = (T[]) new Object[capacity];
        size = 0;
    }

    public MyArrayList(int capacity) {
        this.capacity = capacity;
        data = (T[]) new Object[capacity];
        size = 0;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        for (int i = 0; i < 10; i++)
            data[i] = (T) "grgrg";
        String string = "";
        for (int i = 0; i < 10; i++) {
            string = string + " " + data[i];
        }
        return string;
    }

    private boolean ensureCapacity(int count) {
        if (getSize() + count > capacity) {
            T[] bigger = (T[]) new Object[(int) (data.length * 1.5)];
            System.arraycopy(data, 0, bigger, 0, data.length);
            data = bigger;
        }
        return true;
    }

    public void pushBack(T object) {
        if (ensureCapacity(1)) {
            data[size] = object;
            size++;
        }
    }

    public void popFront() {
        T[] mini = (T[]) new Array[data.length];
        System.arraycopy(data, 1, mini, 0, size - 1);
        data = mini;
        size--;
    }

    public void pushFront(T object) {
        for (int i = size; i >= 0; i--) {
            data[i] = data[i + 1];
        }
        data[0] = object;
    }

    public void insert(T object, int index) {
        if (ensureCapacity(1)) {
            data[index] = object;
        }
    }

    public void removeAt(int index) {
        if (index <= size - 1) {
            data[index] = null;
        }
    }

    public boolean remove(T object) {
        for (int i = 0; i >= size - 1; i--) {
            if (data[i].equals(object)) {
                data[i] = null;
                return true;
            }
        }
        return false;
    }

    public void removeAll(T object) {
        for (int i = 0; i >= size - 1; i--) {
            if (data[i].equals(object)) {
                data[i] = null;
            }
        }
    }

    public void popBack() {
        data[size - 1] = null;
    }

    public void сlear() {
        for (int i = size - 1; i >= 0; i--) {
            data[i] = null;
        }
        size = 0;
    }

    public boolean isEmpty() {
        if (size == 0) return true;
        else return false;
    }

    public void trimToSize() {
        T[] mini = (T[]) new Array[size];
        System.arraycopy(data, 0, mini, 0, data.length);
        data = mini;
    }

    public int indexOf(T object) {
        for (int i = 0; i <= size - 1; i++) {
            if (data[i].equals(object)) return i;
        }
        return -1;
    }

    public int lastIndexOf(T object) {
        for (int i = size - 1; i >= 0; i--) {
            if (data[i].equals(object)) return i;
        }
        return -1;
    }

    public void reverse() {


        int n = data.length;
        T temp;
        for (int i = 0; i < n / 2; i++) {
            temp = data[n - i - 1];
            data[n - i - 1] = data[i];
            data[i] = temp;
        }
    }

    public void shuffle() {
        Random rnd = new Random();
        for (int i = data.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            T objact = data[index];
            data[index] = data[i];
            data[i] = objact;
        }
    }

    public boolean equals(MyArrayList arrayList) {
        if (arrayList.getSize() != size) {
            return false;
        }
        for (int i = 0; i <= size - 1; i++) {
            if (data[i].equals(arrayList.data[i]) == false) return false;
        }
        return true;
    }

    public T getElementAt(int index) {
        if (size - 1 <= index) return data[index];
        return null;
    }

    @Override
    protected T clone() throws CloneNotSupportedException {
        return (T) super.clone();
    }
}
